<!DOCTYPE html>
<html>
<!--
MASAS View Tool
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
-->
<head>
  <title>MASAS Viewing Tool</title>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<%= topCSS %>
	
	
    <%= topJS %>

  
  <script type="text/javascript">

// Path to the blank image should point to a valid location on your server
Ext.BLANK_IMAGE_URL = 'libs/ExtJS-3.4.2/resources/images/default/s.gif';
// Make components not stateful by default
Ext.Component.prototype.stateful = false;
// Assign a state provider
Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
    // 2 years = 730 days
    expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 730))
    //, path: ''
}));
// Tooltips on
Ext.QuickTips.init();
// Path for OpenLayers images
OpenLayers.ImgPath = 'libs/OpenLayers-2.13.1/img/';
// Customize the MASAS Icon location, needs trailing slash
OpenLayers.Format.MASASFeed.IconURL = 'http://icon.masas-sics.ca/';
// Proxy is used for layer loading, feeds use AJAX_PROXY_URL instead
OpenLayers.ProxyHost = '/go?url=';
// Adding retries because default is 0
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
if (!'console' in window || typeof console == 'undefined') {
    // when missing, use a mock console such as OpenLayers' default
    console = OpenLayers.Console;
} else {
    // use the browser's console for logging including from OpenLayers
    OpenLayers.Console = console;
}

// Global namespace
Ext.ns('VIEW');

// Cross-site proxy for interaction with the feed hub(s), comment out when
// loading the sample feed.
//VIEW.AJAX_PROXY_URL = '/go';
// any initial feeds to setup, leave blank if none
VIEW.FEED_SETTINGS = [
    // sandbox2 feed
    //{title: 'Sandbox2', url: 'https://sandbox2.masas-sics.ca/hub/feed'}
    // local sample feed
    {title: 'Sample', url: 'tests/samples/sample-feed.xml', secret: 'sample'}
];
// if a geocoding service is available, provide the URL, otherwise comment
// out to disable this feature
VIEW.ADDRESS_SEARCH_URL = 'address_search';
// the view tool icon location, needs trailing slash
VIEW.ICON_LOCATION = 'img/';
// UA detection should determine if touch screen support is needed
// The OpenLayers mobile stylesheet can also be uncommented and used
//VIEW.TOUCH_ENABLE = true;
// URL for Access Control search
VIEW.ACCESS_CONTROL_SEARCH_URL = 'tests/samples/author/result-name.json';
//VIEW.ACCESS_CONTROL_SEARCH_URL = 'https://access.masas-sics.ca/api/search/';
// Saved author values, should start off with the user's own value so they
// can look at just their own Entries
VIEW.SAVED_AUTHORS = [
    // uri, name, org_name
    ['1', 'NWS Weather', 'National Weather Service']
];
// export/save entry URL, comment out if not available
VIEW.EXPORT_ENTRY_URL = 'export_entry';
// import/upload entry URL, comment out if not available
VIEW.IMPORT_ENTRY_URL = 'import_entry';
// import feed URL
VIEW.IMPORT_FEED_URL = 'import_feed';
// mark an entry as a favorite URL, comment out if not available
VIEW.FAVORITE_ENTRY_URL = 'favorite_entry';
// favorite feed URL
VIEW.FAVORITE_FEED_URL = 'favorite_feed';

  </script>
</head>
<body>
  <div id="north" class="x-hide-display">
    <p>MASAS Viewing Tool - if you can see this, your web browser
does not support this tool.  Please contact us.</p>
  </div>
</body>
  <%= bottomJS %>
</html>
