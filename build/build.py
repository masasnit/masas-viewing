#!/usr/bin/env python
"""
Name:           MASAS Viewing Tool Build
Author:         Jacob Westfall
Created:        Sep 05, 2012
Updated:        Jan 13, 2014
Copyright:      Independent Joint Copyright (c) 2011-2014 MASAS Contributors
License:        Published under the Modified BSD license.  See license.txt for 
                the full text of the license.
Description:	Performs a number of automated build operations.
"""

import os, sys
import optparse
import shutil
from subprocess import PIPE, Popen
try:
    from tools import mergejs
except:
    print "mergejs is required"
    sys.exit(1)
try:
    from tools import jsmin
except:
    print "jsmin is required"
    sys.exit(1)
try:
    from tools import cssmin
except:
    print "cssmin is required"
    sys.exit(1)
if not os.path.isfile("tools/yuicompressor-2.4.7.jar"):
    print "yuicompressor is required"
    sys.exit(1)


# set the relative destination directory, no trailing slash
DEST = "../deploy"  


def build_deploy_dirs(trial=False):
    """ Create any deploy directories needed by other build functions,
    should be run first """
    
    if trial:
        print "Nothing to do"
        return
    
    if not os.path.isdir(DEST):
        os.mkdir(DEST)
    # assumes a relative path and DEST will be added, must be in creation order
    dir_list = [\
        "libs",
        "js",
        "css",
        "libs/ExtJS-3.4.2",
        "libs/ExtJS-3.4.2/css",
        "libs/OpenLayers-2.13.1",
        "libs/GeoExt-1.2rc",
        "libs/GeoExt-1.2rc/css",
    ]
    for each_dir in dir_list:
        new_dir = os.path.join(DEST, each_dir)
        if not os.path.isdir(new_dir):
            print "Creating directory %s" %new_dir
            os.mkdir(new_dir)
    
    print "\nDeployment directories ready"


def build_deploy_files(trial=False):
    """ Copy all files needed for deployment, should be run last """
    
    if trial:
        print "Nothing to do"
        return
    
    # assumes a relative path, specify a source file or directory and
    # destination directory, preceeding directory structure prior to copy will
    # be created, if a directory is specified all sub-dirs will also be copied
    #NOTE: renaming files during copy should also work
    file_list = [\
        ("../libs/ExtJS-3.4.2/adapter/ext/ext-base.js", "libs/ExtJS-3.4.2/"),
        ("../libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js", "libs/ExtJS-3.4.2/"),
        ("../libs/ExtJS-3.4.2/ext-all.js", "libs/ExtJS-3.4.2/"),
        ("../libs/ExtJS-3.4.2/ext-all-debug.js", "libs/ExtJS-3.4.2/"),
        ("../libs/ExtJS-3.4.2/resources/images/default/", "libs/ExtJS-3.4.2/images/default/"),
        ("../libs/ExtJS-3.4.2/resources/images/gray/", "libs/ExtJS-3.4.2/images/gray/"),
        ("../libs/ExtJS-3.4.2/examples/ux/images/x-grouptabs-corners.gif",
            "libs/ExtJS-3.4.2/images/"),
        ("../libs/ExtJS-3.4.2/examples/ux/images/elbow-plus-nl.gif",
            "libs/ExtJS-3.4.2/images/"),
        ("../libs/ExtJS-3.4.2/examples/ux/images/elbow-minus-nl.gif",
            "libs/ExtJS-3.4.2/images/"),
        ("../libs/ExtJS-3.4.2/extensions/images/filter-row-icon.png",
            "libs/ExtJS-3.4.2/images/"),
        ("../libs/ExtJS-3.4.2/extensions/images/filter-row-hrow.gif",
            "libs/ExtJS-3.4.2/images/"),
        ("../libs/OpenLayers-2.13.1/theme/", "libs/OpenLayers-2.13.1/theme/"),
        ("../libs/OpenLayers-2.13.1/img/", "libs/OpenLayers-2.13.1/img/"),
        #("../libs/GeoExt-1.2rc/resources/css/geoext-all.css", "libs/GeoExt-1.2rc/css/"),
        ("../libs/GeoExt-1.2rc/resources/images/default/",
            "libs/GeoExt-1.2rc/images/default/"),
        ("../img/", "img/"),
    ]
    for source,destination in file_list:
        if not os.path.exists(source):
            print "Error: %s doesn't exist" %source
        destination = os.path.join(DEST, destination)
        if os.path.isdir(source):
            # need to remove any existing destination first for copytree
            if os.path.exists(destination):
                shutil.rmtree(destination)
            # let copytree create any directories needed
            shutil.copytree(source, destination,
                ignore=shutil.ignore_patterns("*.bak"))
        else:
            # create necessary directories when copying single files
            dest_dir = os.path.dirname(destination)
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)
            shutil.copy(source, destination)
        print "Copied %s to %s" %(source, destination)
    
    print "\nDeployment files copied"


def build_package(name, source, config, output=None, merger=None, compressor=None,
                  type=None, debug=None, trial_run=False):
    """ Builds a package by merging and compressing a set of files """
    
    # defaults
    if not merger:
        merger = "mergejs"
    if not compressor:
        compressor = "jsmin"
    if not type:
        type = "js"
    
    print "\n-- Building %s Package --" %name
    print "Merging files with %s" %merger
    if merger == "simple":
        # config should instead be a list of filenames to merge
        #NOTE: no trial run available for this merger
        merge_result = []
        for each_file in config:
            file_path = os.path.join(source, each_file)
            print "Exporting: %s" %each_file
            file_header = "/* " + "=" * 70 + "\n   %s\n" %each_file + "   " + \
                "=" * 70 + " */\n\n"
            merge_result.append(file_header)
            with open(file_path) as file_data:
                merge_result.append(file_data.read())
                merge_result.append("\n")
        merged = "".join(merge_result)
    else:
        if trial_run:
            # used to list expected imports and dependencies based on
            # require statements in each javascript file header
            mergejs.run(source, None, config, True)
            print "-- Package Complete --"
            return
        else:
            merged = mergejs.run(source, None, config)
    if debug:
        print "Writing debug output to %s" %debug
        file(debug, "w").write(merged)
    if compressor == "jsmin":
        print "Compressing using jsmin"
        minimized = jsmin.jsmin(merged)
    elif compressor == "cssmin":
        print "Compressing using cssmin"
        minimized = cssmin.cssmin(merged)
    elif compressor == "yui":
        print "Compressing using YUI"
        proc = Popen("java -jar tools/yuicompressor-2.4.7.jar --type %s" %type,
            stdin=PIPE, stdout=PIPE, shell=True)
        minimized, err = proc.communicate(merged)
        if err:
            raise IOError("YUI Error: %s" %err)
    if output:
        print "Writing compressed output to %s" %output
        file(output, "w").write(minimized)
    
    print "-- Package Complete --"


# Packaging functions, prefix of build_ is required

def build_extjs_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return

    source_dir = "../libs/ExtJS-3.4.2"
    config = ["resources/css/ext-all.css"]
    debug_file = "%s/libs/ExtJS-3.4.2/css/ext-all-debug.css" %DEST
    compress_file = "%s/libs/ExtJS-3.4.2/css/ext-all-min.css" %DEST

    build_package("ExtJS CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_extjs_plugins_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return

    source_dir = "../libs/ExtJS-3.4.2"
    config = [\
        "examples/ux/css/GroupTab.css",
        "examples/ux/fileuploadfield/css/fileuploadfield.css",
        "extensions/css/FilterRow.css",
    ]
    debug_file = "%s/libs/ExtJS-3.4.2/css/view-plugins-debug.css" %DEST
    compress_file = "%s/libs/ExtJS-3.4.2/css/view-plugins-min.css" %DEST

    build_package("ExtJS Plugins CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_extjs_plugins(trial=False):

    source_dir = "../libs/ExtJS-3.4.2"
    config_file = "extjs-view.cfg"
    debug_file = "%s/libs/ExtJS-3.4.2/view-plugins-debug.js" %DEST
    compress_file = "%s/libs/ExtJS-3.4.2/view-plugins-min.js" %DEST

    build_package("ExtJS", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_openlayers(trial=False):

    source_dir = "../libs/OpenLayers-2.13.1/lib"
    config_file = "openlayers.cfg"
    debug_file = "%s/libs/OpenLayers-2.13.1/OpenLayers-debug.js" %DEST
    compress_file = "%s/libs/OpenLayers-2.13.1/OpenLayers.js" %DEST

    build_package("OpenLayers", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_geoext_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return

    source_dir = "../libs/GeoExt-1.2rc/"
    config = [\
        "resources/css/popup.css",
        "resources/css/layerlegend.css",
        "resources/css/symbolizercolumn.css",
    ]
    debug_file = "%s/libs/GeoExt-1.2rc/css/geoext-all-debug.css" %DEST
    compress_file = "%s/libs/GeoExt-1.2rc/css/geoext-all-min.css" %DEST

    build_package("GeoExt CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_geoext(trial=False):

    source_dir = "../libs/GeoExt-1.2rc/lib"
    config_file = "geoext.cfg"
    debug_file = "%s/libs/GeoExt-1.2rc/GeoExt-debug.js" %DEST
    compress_file = "%s/libs/GeoExt-1.2rc/GeoExt-min.js" %DEST
    
    build_package("GeoExt", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_xmljson(trial=False):

    source_dir = "../libs"
    config_file = "xmljson.cfg"
    debug_file = "%s/libs/xml-json-debug.js" %DEST
    compress_file = "%s/libs/xml-json-min.js" %DEST

    build_package("XMLJSON", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_overrides(trial=False):

    source_dir = "../js"
    config_file = "overrides-view.cfg"
    debug_file = "%s/js/view-overrides-debug.js" %DEST
    compress_file = "%s/js/view-overrides-min.js" %DEST

    build_package("Overrides", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_application_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return

    source_dir = "../css"
    config = ["view.css"]
    debug_file = "%s/css/view-debug.css" %DEST
    compress_file = "%s/css/view-min.css" %DEST

    build_package("Application CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_application(trial=False):

    source_dir = "../js"
    config_file = "view.cfg"
    debug_file = "%s/js/view-debug.js" %DEST
    compress_file = "%s/js/view-min.js" %DEST

    build_package("Application", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


if __name__ == '__main__':

    desc = """  These are the available modules that can be individually built:
    deploy_dirs, deploy_files, extjs_css, extjs_plugins_css, extjs_plugins,
    openlayers, geoext_css, geoext, xmljson, overrides, application_css, application
    """
    parser = optparse.OptionParser(description=desc)
    parser.add_option("-d", help="set an alternate destination, relative or absolute",
        dest="new_dest", action="store")
    parser.add_option("-c", help="make clean by removing old deployment",
        dest="clean", default=False, action="store_true")
    parser.add_option("-p", help="build a single package",
        dest="package", action="store")
    parser.add_option("-t", help="trial run of a single package only",
        dest="trial", default=False, action="store_true")
    (opts, args) = parser.parse_args()
    
    if opts.new_dest:
        DEST = opts.new_dest
        print "Setting new destination to %s\n" %opts.new_dest
    if opts.clean:
        print "Removing old deployment\n"
        shutil.rmtree(DEST)
    if opts.trial and not opts.package:
        parser.error("You must specify a package for the trial run")
    if opts.package:
        mod = __import__(__name__)
        if not hasattr(mod, "build_%s" %opts.package):
            parser.error("Unable to find %s package" %opts.package)
        getattr(mod, "build_%s" %opts.package)(opts.trial)
    else:
        # these are in the correct build order
        build_deploy_dirs()
        build_extjs_css()
        build_extjs_plugins_css()
        build_extjs_plugins()
        build_openlayers()
        build_geoext_css()
        build_geoext()
        build_xmljson()
        build_overrides()
        build_application_css()
        build_application()
        build_deploy_files()
