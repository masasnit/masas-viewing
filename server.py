#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           Development Web Server
Author:         Jacob Westfall
Created:        Jan 24, 2011
Updated:        Jan 13, 2014
Copyright:      Independent Joint Copyright (c) 2011-2014 MASAS Contributors
License:        Published under the Modified BSD license.  See license.txt for 
                the full text of the license.
Description:    This application serves html files locally and via a proxy.
"""

import os, sys
import urllib2
import StringIO
import zipfile
import xml.dom.minidom as minidom
import mimetypes
import datetime
import re

import cherrypy

if sys.version < "2.6" or sys.version > "2.7":
    print "Python 2.6 or 2.7 is required"
    sys.exit(1)

# get absolute path name from executable   
server_pathname = os.path.dirname(sys.argv[0])
# change directory to absolute path name
os.chdir(os.path.abspath(server_pathname))


class RootPage(object):
    """ Viewing Tool Server Root """
    
    def __init__(self):
        """ initial setup """
        
        # common types allowed on a Hub
        self.attachment_type_extension = {
            "image/jpeg": ".jpg",
            "image/png": ".png",
            "image/gif": ".gif",
            "image/svg+xml": ".svg",
            "text/plain": ".txt",
            "text/rtf": ".rtf",
            "application/pdf": ".pdf",
            "application/vnd.ms-powerpoint": ".ppt",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation": ".pptx",
            "application/msword": ".doc",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document": ".docx",
            "application/vnd.oasis.opendocument.text": ".odt",
            "application/vnd.oasis.opendocument.presentation": ".odp",
            "application/vnd.google-earth.kml+xml": ".kml",
            "audio/wav": ".wav",
            "audio/amr": ".amr",
        }
        # mimetypes is used as a backup
        mimetypes.init()
        
        self.import_entries = []
        self.import_attachments = []
        self.favorite_entries = []
    
    
    @cherrypy.expose
    def index(self):
        """ Create a recursive listing of all available web pages """
        
        html_files = []
        startDir = "."
        directories = [startDir]
        while len(directories) > 0:
            directory = directories.pop()
            for name in os.listdir(directory):
                fullpath = os.path.join(directory, name)
                if os.path.isfile(fullpath):
                    html_files.append(fullpath)
                elif os.path.isdir(fullpath):
                    directories.append(fullpath)
        
        html = ["<html>"]
        for new_file in html_files:
            html.append("""<a href="/%s"> %s </a>""" %(new_file, new_file))
            html.append("<br>")
        html.append("</html>")
        
        return str("\n".join(html))
    
    
    @cherrypy.expose
    def go(self, *args, **kwargs):
        """ Proxy for cross-domain javascript """
        
        if "url" not in kwargs:
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 3"
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 4"
        # default is 30 seconds which should be long enough to fetch a Feed
        # or an Entry, any longer and the user will wonder what is happening.
        # For POST or GET of other content, supplying a longer timeout
        # is possible because those operations will take longer with large
        # files, ie 3 minutes for 10 MB on most DSL service
        request_timeout = 30
        if "timeout" in kwargs:
            try:
                request_timeout = int(kwargs["timeout"])
                # cherrypy default is 5 mins max for a request
                if request_timeout > 300:
                    raise ValueError
            except:
                cherrypy.response.headers["Content-Type"] = "text/plain"
                return "\nIllegal Request, Error: 5"
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Viewing Tool Proxy"}
            if "Authorization" in cherrypy.request.headers:
                new_headers["Authorization"] = cherrypy.request.headers["Authorization"]
            if cherrypy.request.method == "POST":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # There is a bug in older python versions < 2.5 where only 200
                # is an acceptable response vs 201
            elif cherrypy.request.method == "PUT":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # consider using httplib2 which has builtin PUT method instead
                new_request.get_method = lambda: 'PUT'
            else:
                new_request = urllib2.Request(url, headers=new_headers)
            new_connection = urllib2.urlopen(new_request, timeout=request_timeout)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def address_search(self, *args, **kwargs):
        """ Return a JSON listing of geocoded addresses, sample data provided """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        return """{"results": [{"address": "Main Street", "lat": "42.5", "lon": "-82"}] }"""
    
    
    @cherrypy.expose
    def export_entry(self, *args, **kwargs):
        """ Save the Entry and return a ZIP.  This can take a long time to
        download not only the Entry but any attachments as well. """
        
        if "url" not in kwargs:
            raise cherrypy.HTTPError("400", "URL is missing")
        if not kwargs["url"]:
            raise cherrypy.HTTPError("400", "URL is missing")
        if "secret" not in kwargs:
            raise cherrypy.HTTPError("400", "Secret is missing")
        if not kwargs["secret"]:
            raise cherrypy.HTTPError("400", "Secret is missing")
        try:
            # 30 seconds to download this Entry
            entry = url_download("%s?secret=%s" %(kwargs["url"],
                kwargs["secret"]), 30)
        except:
            raise cherrypy.HTTPError("400", "Unable to download entry URL")
        try:
            new_entry,attachments = self.find_entry_attachments(entry)
        except:
            raise cherrypy.HTTPError("400", "Unable to parse entry XML")
        # zipfile needs a file like object to work with
        output = StringIO.StringIO()
        archive = zipfile.ZipFile(output, 'w', zipfile.ZIP_DEFLATED)
        try:
            archive.writestr("entry.xml", new_entry)
            if attachments:
                for a_url,a_name in attachments:
                    # 3 minutes allotted for attachment download since it
                    # should be enough time for a 10 MB file on DSL
                    a_data = url_download("%s?secret=%s" %(a_url,
                        kwargs["secret"]), 180)
                    archive.writestr(a_name, a_data)
        except:
            archive.close()
            output.close()
            raise cherrypy.HTTPError("400", "Unable to add Entry or Attachments")
        try:
            # need to close the archive first before contents can be read
            archive.close()
            contents = output.getvalue()
            output.close()
        except:
            archive.close()
            output.close()
            raise cherrypy.HTTPError("400", "Unable to build ZIP file")
        
        cherrypy.response.headers["Content-Type"] = "application/zip"
        cherrypy.response.headers["Content-Disposition"] = 'attachment; filename="entry.zip"'
        
        return contents
    
    
    def find_entry_attachments(self, entry):
        """ Parse the Entry and return a modified version with attachments """
        
        attachments = []
        doc = minidom.parseString(entry)
        entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
            "entry")[0]
        if not entry_element:
            raise ValueError("Not an Entry")
        for link in doc.getElementsByTagName("link"):
            if link.getAttribute("rel") == "enclosure":
                link_url = link.getAttribute("href")
                link_type = link.getAttribute("type")
                # common Hub types
                extension = self.attachment_type_extension.get(link_type, None)
                if not extension:
                    # fallback to mimetypes or a default
                    extension = mimetypes.guess_extension(link_type)
                    if not extension:
                        extension = ".xml"
                # should result in "content/0.jpg"
                new_link = "content/" + link_url.split("/content/")[1] + extension
                # saving original for download and new for zip filename
                attachments.append((link_url, new_link))
                # update in the entry XML so that its now a relative URL
                link.setAttribute("href", new_link)
        new_entry = doc.toxml(encoding="utf-8")
        
        return new_entry, attachments
    
    
    @cherrypy.expose
    def import_entry(self, *args, **kwargs):
        """ Import a zip file containing an Entry and any associated
        attachments, adding to the user's import feed """
        
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        # really only care about the file upload data right now, ignoring
        # details like the filename, etc
        try:
            entry_zip = zipfile.ZipFile(kwargs["attachment-file"].file, "r")
        except:
            return "{ success: false, message: 'Not a ZIP file.' }"
        try:
            entry_xml = entry_zip.read("entry.xml")
        except:
            entry_zip.close()
            return "{ success: false, message: 'Entry is missing.' }"
        try:
            doc = minidom.parseString(entry_xml)
            entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
                "entry")[0]
            if not entry_element:
                raise ValueError("Entry element missing")
            # get a placeholder location
            self.import_entries.append(None)
            entry_pos = len(self.import_entries) - 1
            for link in doc.getElementsByTagName("link"):
                link_rel = link.getAttribute("rel")
                link_href = link.getAttribute("href")
                if link_rel == "edit":
                    # save the original link, assumably to a Hub entry
                    old_link = doc.createElement("link")
                    old_link.setAttribute("rel", "related")
                    old_link.setAttribute("title", "Original Entry Link")
                    old_link.setAttribute("href", link_href)
                    link.parentNode.appendChild(old_link)
                    # replace with the local relative link
                    link.setAttribute("href", "import_feed?entry=%s" %entry_pos)
                elif link_rel == "enclosure":
                    # attachment links should be relative filenames that
                    # are available in the zip file
                    self.import_attachments.append((link.getAttribute("type"),
                        entry_zip.read(link_href)))
                    attach_pos = len(self.import_attachments) - 1
                    # replace with local relative links too
                    link.setAttribute("href", "import_feed?attachment=%s" %attach_pos)
                elif link_rel == "related":
                    pass
                else:
                    # other link such as history or edit-media, which won't be
                    # valid and need to be removed
                    link.parentNode.removeChild(link)
        except:
            return "{ success: false, message: 'Invalid Entry.' }"
        finally:
            entry_zip.close()
        # saving the modified version of the entry as a standalone entry
        # with declaration and namespaces
        self.import_entries[entry_pos] = doc.toxml(encoding="utf-8")
        
        return "{ success: true }"
    
    
    @cherrypy.expose
    def import_feed(self, *args, **kwargs):
        """ A user's import feed containing any Entries they have uploaded """
        
        if "entry" in kwargs:
            # direct access to an Entry
            try:
                entry_location = int(kwargs["entry"])
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=entry;charset="utf-8"'
                
                return self.import_entries[entry_location]
            except:
                raise cherrypy.HTTPError("400", "Entry Error")
        elif "attachment" in kwargs:
            # direct access to an Attachment
            try:
                attach_location = int(kwargs["attachment"])
                cherrypy.response.headers["Content-Type"] = \
                    self.import_attachments[attach_location][0]
                
                return self.import_attachments[attach_location][1]
            except:
                raise cherrypy.HTTPError("400", "Attachment Error")
        else:
            # generate a feed of any imported Entries
            try:
                feed = []
                declaration_match = re.compile(r"<\?xml.*\?>")
                entry_match = re.compile(r"<entry.*>")
                # start feed with minimum values
                feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Import Feed</name>
    </author>
    <id>import-feed</id>
    <link href="import_feed" rel="self" />
    <title type="text">Import Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
                # add revised entries suitable for a feed
                for entry in self.import_entries:
                    feed_entry = declaration_match.sub("", entry)
                    feed_entry = entry_match.sub("<entry>", feed_entry)
                    feed.append(feed_entry)
                # close feed
                feed.append("\n</feed>")
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=feed;charset="utf-8"'
                
                return "\n".join(feed)
            except:
                raise cherrypy.HTTPError("400", "Feed Error")
    
    
    @cherrypy.expose
    def favorite_entry(self, *args, **kwargs):
        """ Add this Entry to the favorites list """
        
        if "url" not in kwargs:
            raise cherrypy.HTTPError("400", "URL is missing")
        if not kwargs["url"]:
            raise cherrypy.HTTPError("400", "URL is missing")
        if "secret" not in kwargs:
            raise cherrypy.HTTPError("400", "Secret is missing")
        if not kwargs["secret"]:
            raise cherrypy.HTTPError("400", "Secret is missing")
        try:
            # 30 seconds to download this Entry
            entry = url_download("%s?secret=%s" %(kwargs["url"],
                kwargs["secret"]), 30)
        except:
            raise cherrypy.HTTPError("400", "Unable to download entry URL")
        try:
            doc = minidom.parseString(entry)
            entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
                "entry")[0]
            if not entry_element:
                raise ValueError("Entry element missing")
            # save XML again to ensure uniformity for re matches
            entry_xml = doc.toxml(encoding="utf-8")
        except:
            raise cherrypy.HTTPError("400", "Unable to parse entry XML")
        try:
            declaration_match = re.compile(r"<\?xml.*\?>")
            entry_match = re.compile(r"<entry.*>")
            feed_entry = declaration_match.sub("", entry_xml)
            feed_entry = entry_match.sub("<entry>", feed_entry)
            self.favorite_entries.append(feed_entry)
        except:
            raise cherrypy.HTTPError("400", "Unable to add to favorites")
    
    
    @cherrypy.expose
    def favorite_feed(self, *args, **kwargs):
        """ A user's favorite feed containing any Entries they have selected """
        
        if "reset" in kwargs and kwargs["reset"] == "yes":
            self.favorite_entries = []
            return
        
        try:
            feed = []
            feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Favorite Feed</name>
    </author>
    <id>favorite-feed</id>
    <link href="favorite_feed" rel="self" />
    <title type="text">Favorite Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
            feed.extend(self.favorite_entries)
            feed.append("\n</feed>")
            cherrypy.response.headers["Content-Type"] = \
                'application/atom+xml;type=feed;charset="utf-8"'
            
            return "\n".join(feed)
        except:
            raise cherrypy.HTTPError("400", "Feed Error")


## Misc Functions


def url_download(url, request_timeout):
    """ Download a provided URL """
    
    connection = None
    try:
        proxy_headers = {"User-Agent": "MASAS Viewing Tool Proxy"}
        request = urllib2.Request(url, headers=proxy_headers)
        connection = urllib2.urlopen(request, timeout=request_timeout)
        # 10 MB max size on most Hubs
        data = connection.read(10485760)
        return data
    finally:
        if connection:
            connection.close()



def run():
    """ Run method for standalone server"""
    
    server_config = {"server.socket_host": "0.0.0.0", "server.socket_port": 8080,
        # set to 10MB to prevent oversize uploads
        "server.max_request_body_size": 10485760}
    app_config = {"/": {"tools.staticdir.root": os.path.abspath(server_pathname),
        "tools.staticdir.on": True, "tools.staticdir.dir": "."}}
    cherrypy.config.update(server_config)
    cherrypy.tree.mount(RootPage(), "/", app_config)
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run()
