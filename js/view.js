/**
MASAS View Tool - Viewport Setup - Application Init
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains overall application settings and controls the 
initialization sequence for the viewer.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * On document ready, initialize the app
 */
Ext.onReady(function () {
    
    // initializes the map, layers, controls, and toolbars
    VIEW.initialize_map();
    
    // create map panel
    VIEW.mapPanel = new GeoExt.MapPanel({
        region: 'center',
        map: VIEW.map,
        center: new OpenLayers.LonLat(-94.0, 52.0).transform(VIEW.mapOptions.displayProjection,
            VIEW.mapOptions.projection),
        zoom: 4,
        tbar: VIEW.mapToolbarTop,
        bbar: VIEW.mapToolbarBottom,
        // make stateful so the map center and zoom will be saved
        stateful: true,
        // ignore other stateEvents defined by GeoExt for MapPanel
        stateEvents: [],
        // used for state cookies
        stateId: 'map'
    });
    
    // create the feed source store via the constructor which then adds to
    // the Ext.StoreMgr this new storeId of 'FeedSources'
    var feed_store = new VIEW.FeedSourceStore({data: VIEW.FEED_SETTINGS});
    
    // create the view components
    VIEW.header = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Viewing Tool - Sandbox</div>' +
            '<div id="headerDetailBlock">Version 0.4</div>'
    });
    VIEW.settingsPanel = VIEW.Panel.SettingsPanel();
    VIEW.gridPanel = new VIEW.FeedGrid({layer: VIEW.mapLayers.MASASLayer});
    // main layout view, uses entire browser viewport
    VIEW.mainView = new Ext.Viewport({
        layout: 'border',
        items: [VIEW.header, VIEW.gridPanel, VIEW.settingsPanel, VIEW.mapPanel]
    });

});
