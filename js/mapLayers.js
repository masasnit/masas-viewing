/**
MASAS View Tool - Map Layers
Updated: Jul 15, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings for the top toolbar and
map layers.
*/

/*global VIEW,Ext,OpenLayers,GeoExt,google */

Ext.ns('VIEW.MapLayers');

/**
 * Setup map layers once application loaded and ready
 */
VIEW.MapLayers.initialize = function () {
    
    /* Base Layers */
    
    var base_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts'],
        ['OSM Tile Service', 'osm'],
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest'],
        ['XYZ Grid Map Service', 'xyz' ]
    ];
    
    var base_layer_fields = [
        // required fields for all layer types, the LayerRecord already has
        // defined 'layer' and 'title' with 'name' mapping to title.
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // optional fields
        // use "defaultValue" to set a (non-falsy) value for something thats missing
        {name: 'base_layer', type: 'boolean', defaultValue: true},
        // layers that can't be deleted, defaults for all users
        {name: 'permanent', type: 'boolean'},
        {name: 'map_type', type: 'string'},
        // allows overriding the map default of 16 depending on the layer
        {name: 'zoom_levels', type: 'integer'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'showOnToolbar', type: 'boolean'},
        // holds references to the toolbar button and layers menu item
        {name: 'button'},
        {name: 'menu'}
    ];
    
    VIEW.MapLayers.baseLayers = new VIEW.Layers.LayerStore({
        'storeId': 'baseLayers',
        'map': VIEW.map,
        'fields': base_layer_fields,
        'layer_types': base_layer_types,
        'base_layers': true
    });

    // Load the layers...
    VIEW.MapLayers.loadBaseLayers();

    // start out with the first available layer
    if( VIEW.MapLayers.baseLayers.getCount() > 0 ) {
        VIEW.map.setBaseLayer( VIEW.MapLayers.baseLayers.getAt(0).getLayer() );
    }
    
    /* Overlay Layers */
    
    var overlay_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts']
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest'],
        ['KML', 'application/vnd.google-earth.kml+xml']
    ];
    
    var overlay_layer_fields = [
        {name: 'visibility', type: 'boolean'},
        //NOTE: HTTP Basic Auth urls should contain "https://username:password@server"
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // in the store its a string, but is converted to float for layer
        {name: 'opacity', type: 'string'},
        {name: 'permanent', type: 'boolean'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'showOnToolbar', type: 'boolean'},
        {name: 'button'},
        {name: 'menu'}
    ];
    
    VIEW.MapLayers.overlayLayers = new VIEW.Layers.LayerStore({
        'storeId': 'overlayLayers',
        'map': VIEW.map,
        'fields': overlay_layer_fields,
        'layer_types': overlay_layer_types
    });

    // Load the overlays...
    VIEW.MapLayers.loadOverlayLayers();

    /* Entry Layers */
    
    var entry_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts'],
        ['Entry Geometry', 'entry']
    ];
    
    var entry_layer_fields = [
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'opacity', type: 'string'},
        // used for entry geometry data
        {name: 'geom_type', type: 'string'},
        {name: 'geom_val', type: 'string'}
    ];
    
    VIEW.MapLayers.entryLayers = new VIEW.Layers.LayerStore({
        'storeId': 'entryLayers',
        'map': VIEW.map,
        'fields': entry_layer_fields,
        'layer_types': entry_layer_types
    });
    
    /* Layer menus */
    var layers_menu = VIEW.MapLayers.createDefaultLayersMenu();

    /* Top toolbar on the map */
    VIEW.MapLayers.createDefaultToolbar();

    VIEW.mapToolbarTop = {
        // allow extra items to be seen using a dropdown menu
        enableOverflow: true,
        // allow mouse users to click and drag to reorder items
        //TODO: should this be turned off for touch users ?
        plugins : [
            new Ext.ux.ToolbarReorderer({defaultReorderable: true})
        ],
        
        // custom state saving for this toolbar, allowing the user to populate
        // the toolbar with default and custom layers and save/restore them
        stateful: true,
        stateId: 'toolbar',
        getState: function () {
            var buttons = {};
            var button_count = 0;
            this.items.each(function (item) {
                // ignore other toolbar items like spacers, menus, etc
                if (item['layer_name']) {
                    button_count += 1;
                    // saves enough data to either link to an existing layer
                    // or create a user defined one
                    buttons['button' + button_count] = {
                        'name': item['layer_name'],
                        'type': item['layer_type'],
                        'url': item['layer_url'],
                        'store': item['layer_store']
                    };
                }
            });
            
            return {'buttons': buttons, 'count': button_count};
        },
        applyState: function (state) {
            if (state && state.count && state.count > 0) {
                var saved_buttons = [];
                for (var i = 1; i <= state.count; i++) {
                    if (state.buttons['button' + i]) {
                        saved_buttons.push(state.buttons['button' + i]);
                    }
                }
                if (saved_buttons.length > 0) {
                    // replace the default with user defined list
                    VIEW.MapLayers.toolbar_buttons = saved_buttons;
                }
                console.debug('Restored ' +  saved_buttons.length +
                    ' toolbar buttons');
            }
        },
        
        items: [{
            xtype: 'tbspacer',
            width: 40,
            // don't allow the toolbar to reorder this item
            reorderable: false
        },
        // using a factory method in FeedGrid.js to create this button for both
        // the grid and map toolbars
        VIEW.generate_layout_button(true),
        {
            text: 'Layers',
            reorderable: false,
            menu: layers_menu
        }, {
            xtype: 'tbspacer',
            width: 20,
            reorderable: false
        }],
        
        listeners: {
            // once the toolbar has been rendered and the state restore is
            // done, the buttons can be added
            afterrender: function (cmp) {
                var base_separator = false;
                for (var i = 0; i < VIEW.MapLayers.toolbar_buttons.length; i++) {
                    var btn_data = VIEW.MapLayers.toolbar_buttons[i];
                    var layer_store = Ext.StoreMgr.get(btn_data.store);
                    if (!layer_store) {
                        console.error('Unable to load layer store ' + btn_data.store);
                        continue;
                    }
                    // base layers should be on the left side with a separator.
                    // Once all of them have been added, use a separator prior
                    // to any overlays, assuming that is the order
                    if (btn_data.store === 'baseLayers') {
                        base_separator = true;
                    } else {
                        if (base_separator) {
                            cmp.addSeparator();
                            base_separator = false;
                        }
                    }
                    // either use an existing layer or add a new custom layer
                    var layer_idx = layer_store.findExact('url', btn_data.url);
                    if (layer_idx === -1) {
                        layer_store.loadData(btn_data, true);
                        layer_idx = layer_store.findExact('url', btn_data.url);
                    }
                    var layer_record = layer_store.getAt(layer_idx);
                    VIEW.Layers.add_toolbar_button(layer_record, cmp);
                    // silently update the menu to show the layer is on the toolbar
                    var layer_menu = layer_record.get('menu');
                    if (layer_menu) {
                        layer_menu.menu.toolbarCheckItem.setChecked(true, true);
                    }
                }
                // call doLayout to refresh the view which causes any
                // unrendered child Components to be rendered
                VIEW.mapPanel.doLayout();
            }
        }
    };
};

/**
 * Loads the base layers from a file.
 */
VIEW.MapLayers.loadBaseLayers = function() {
    var filteredLayers = [];

    var allowGoogleLayers = true;

    // Google Maps is normally the default due to being listed first,
    // however it may fail to load, which can happen with some networks with
    // restrictive firewalls.  In that case, OSM becomes the fallback default
    if( !( 'google' in window ) )
    {
        allowGoogleLayers = false;

        console.error('Unable to load google maps, OSM fallback');
        alert('A problem with your network prevented the Google Map from ' +
            'loading properly.  Open Street Map will be used instead.  If ' +
            'you still want to use the Google Map, you can reload this ' +
            'webpage to try again.');
    }

    // Load the layers...
    var layers = VIEW.MapLayers.loadLayersFromFile( "./config/layers_base.json", "base_layers" );

    // Let's add the maps the the base_data structure...
    for( var baseLayerCtr = 0; baseLayerCtr < layers.length; baseLayerCtr++ )
    {
        var curLayer = layers[baseLayerCtr];
        var layerEnabled = true;

        // Check the Enabled property...
        if( curLayer.hasOwnProperty( "enabled") ) {
            layerEnabled = curLayer.enabled;
        }

        // If we have a "google" layer but with no support...
        if( curLayer.type == "google" && allowGoogleLayers == false )
        {
            // Don't add it!
            layerEnabled = false;
        }

        if( layerEnabled == true ) {
            filteredLayers.push( curLayer );
        }
    }

    if( filteredLayers.length == 0 )
    {
        console.error( 'No base maps were found/enabled.  Will use a default base map!' );
        alert( "A base map has not been properly configure: Please contact the Administrator. A default map will be used instead." )

        // We need a base map... otherwise it looks bad!
        filteredLayers.push( {
            "name": "OpenStreetMap",
            "url": "http://otile1.mqcdn.com/tiles/1.0.0/map/",
            "type": "osmm",
            "visibility": false,
            "base_layer": true,
            "permanent": true,
            "zoom_levels": 19,
            "attribution": "MapQuest-OSM",
            "description": "OpenStreetMap tiles from MapQuest",
            "showOnToolbar": true
        } );
    }

    VIEW.MapLayers.baseLayers.loadData( filteredLayers );
};

/**
 * Loads the overlay layers from a file.
 */
VIEW.MapLayers.loadOverlayLayers = function() {
    var filteredLayers = [];

    // Load the layers...
    var layers = VIEW.MapLayers.loadLayersFromFile( "./config/layers_overlays.json", "overlay_layers" );

    // Let's add the maps the the base_data structure...
    for( var baseLayerCtr = 0; baseLayerCtr < layers.length; baseLayerCtr++ )
    {
        var curLayer = layers[baseLayerCtr];
        var layerEnabled = true;

        // Check the Enabled property...
        if( curLayer.hasOwnProperty( "enabled") ) {
            layerEnabled = curLayer.enabled;
        }

        if( layerEnabled == true ) {
            filteredLayers.push( curLayer );
        }
    }

    VIEW.MapLayers.overlayLayers.loadData( filteredLayers );
};

/**
 * Loads an array of objects from a file
 * @param fileName The file to load the objects from.
 * @param attribute The attribute name of the array to return
 * @returns {Array} The array of objects if exists/available, empty otherwise.
 */
VIEW.MapLayers.loadLayersFromFile = function( fileName, attribute ) {
    var layers = [];

    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", fileName, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        var respJSON = JSON.parse( responseTxt );

        if( respJSON.hasOwnProperty( attribute ) ) {
            layers = respJSON[attribute];
        }
    }

    return layers;
};

/**
 * Create the default layers menu.
 *
 * @returns {Ext.menu.Menu}
 */
VIEW.MapLayers.createDefaultLayersMenu = function() {

    var base_menu = new VIEW.Layers.LayerMenu({
        layers: VIEW.MapLayers.baseLayers,
        createNewLayers: true
    });

    var overlay_menu = new VIEW.Layers.LayerMenu({
        layers: VIEW.MapLayers.overlayLayers,
        createNewLayers: true
    });

    var layers_menu = new Ext.menu.Menu({
        ignoreParentClicks: true,
        plain: true,
        items: [{
            text: 'Base Layer',
            menu: base_menu
        }, '-', {
            text: 'OverLays',
            menu: overlay_menu
        }]
    });

    if( VIEW.MapLayers.entryLayers.totalLength > 0 ) {
        var entry_menu = new VIEW.Layers.LayerMenu({
            layers: VIEW.MapLayers.entryLayers
        });

        if( layers_menu.items.getCount() > 0 ) {
            layers_menu.addSeparator();
        }

        layers_menu.addMenuItem({
            text: 'Entry Layers',
            menu: entry_menu
        });
    }

    return layers_menu;
};

/**
 * Create the default toolbar.
 */
VIEW.MapLayers.createDefaultToolbar = function() {

    // default buttons if no user selected buttons
    VIEW.MapLayers.toolbar_buttons = [];

    // Check the what base layers need to be added to the toolbar...
    for( var baseLayersCtr = 0; baseLayersCtr < VIEW.MapLayers.baseLayers.getCount(); baseLayersCtr++ )
    {
        var baseLayer = VIEW.MapLayers.baseLayers.getAt( baseLayersCtr );

        if( baseLayer.data.hasOwnProperty( "showOnToolbar" ) )
        {
            VIEW.MapLayers.toolbar_buttons.push({
                'name': baseLayer.data.name,
                'type': baseLayer.data.type,
                'url': baseLayer.data.url,
                'store': 'baseLayers'
            });
        }
    }

    // Check the what overlay layers need to be added to the toolbar...
    for( var overlayLayersCtr = 0; overlayLayersCtr < VIEW.MapLayers.baseLayers.getCount(); overlayLayersCtr++ )
    {
        var overlayLayer = VIEW.MapLayers.overlayLayers.getAt( overlayLayersCtr );

        if( overlayLayer.data.hasOwnProperty( "showOnToolbar" ) )
        {
            VIEW.MapLayers.toolbar_buttons.push({
                'name': overlayLayer.data.name,
                'type': overlayLayer.data.type,
                'url': overlayLayer.data.url,
                'store': 'overlayLayers'
            });
        }
    }

};
