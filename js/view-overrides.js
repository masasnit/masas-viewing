/**
MASAS View Tool - ExtJS and OpenLayers overrides
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

/**
 * Adds the values and keys functions for objects
 * Doesn't overwite if already has these functions
 */
Ext.applyIf(Ext, {
    getKeys: function (obj) {
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    },
    getValues: function (obj) {
        var vals = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                vals.push(obj[k]);
            }
        }
        return vals;
    }
});


/**
 * Add the additional 'advanced' VTypes
 */
Ext.apply(Ext.form.VTypes, {
    daterange : function (val, field) {
        var date = field.parseDate(val);
        if (!date) {
            return false;
        }
        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = Ext.getCmp(field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = Ext.getCmp(field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        return true;
    }
});


/**
 * Setup some default getState,applyState methods for forms, used to save
 * feed settings state
 */
Ext.override(Ext.form.FormPanel, {
    getState: function () {
        return this.form.getState();
    },
    applyState: function (state) {
        this.form.applyState(state);
    }
});

Ext.override(Ext.form.BasicForm, {
    getState: function () {
        var state = {};
        var getStateValue = function (f) {
            if (f instanceof Ext.form.Radio) {
                if (f.getValue()) {
                    state[f.name || f.id] = true;
                }
            }
            else if (f instanceof Ext.form.CompositeField) {
                f.items.each(getStateValue);
            }
            else if (f instanceof Ext.form.ComboBox) {
                
                var rec = f.findRecord(f.displayField, f.getValue());
                if (!rec) {
                    rec = f.findRecord(f.valueField, f.getValue());
                }
                state[f.name || f.id] = rec ? rec.get(f.valueField) : f.listEmptyText;
            }
            else {
                try {
                    if (f.inputType && f.inputType == 'password' && f.orginalValue) {
                        state[f.name || f.id] = f.originalValue;
                    }
                    else {
                        state[f.name || f.id] = f.getValue();
                    }   
                }
                catch (e) {
                    state[f.name || f.id] = f.value;
                }
            }
        };
        this.items.each(getStateValue);
        return state;
    },
    /*
    getState: function () {
        return this.getFieldValues();
    },
    */
    applyState: function (state) {
        this.setValues(state);
    }
});

/**
 * Default getState implimentation for checkboxes
 */
Ext.override(Ext.form.Checkbox, {
    getState: function () {
        return {checked: this.getValue()};
    }
});


/**
 * Add the fix to keep Google Maps V3 Layers from being initially hidden
 */
/* does not appear to be needed with OpenLayers 2.13.1
OpenLayers.Layer.Google.prototype.initialize = Ext.createInterceptor(OpenLayers.Layer.Google.prototype.initialize,
    function (name, options) {
        if (!options) {
            options = {eventListeners: {}};
        }
        else if (!options.eventListenters) {
            options.eventListeners = {};
        }
        Ext.apply(options.eventListeners, {
            // using moveend because it only fires on visible layers
            moveend: function (evt) {
                var lyr = evt.object;
                if (!lyr.visFixApplied) {
                    var gmapDiv = Ext.getDom(lyr.map.id + "_GMapContainer");
                    var gmapEl = new Ext.Element(gmapDiv);
                    evt.object.visFixApplied = true;
                    var task = Ext.TaskMgr.start({
                        run: function (elem) {
                            if (elem.dom.style.left) {
                                elem.setLeft('');
                                Ext.TaskMgr.stop(task);
                            }
                        },
                        args: [gmapEl],
                        interval: 250,
                        repeat: 40 //run for up to 10 sec
                    });
                }
            }
        });
    }
);
*/
