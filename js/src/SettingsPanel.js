/**
MASAS View Tool - SettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file displays a panel which contains all the other settings panels
(user settings, feed, time, author and advanced settings).  This is an
accordion panel and may be collapsed.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * Settings panel, create & configure using a factory
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.SettingsPanel = function (config) {
    return new Ext.Panel(Ext.apply({
        title: 'Settings',
        // left side of the overall layout
        region: 'west',
        // Ext.SplitBar allows the user to click and drag the top of the
        // grid to resize it. Avoid inadvertant use by touch users.
        split: (VIEW.TOUCH_ENABLE) ? false : true,
        collapsible: true,
        //TODO: easier to collapse but could it cause inadvertant touches
        //      by users trying to get to the User settings?
        titleCollapse: true,
        width: 300,
        layout: {
            type: 'accordion',
            animate: true
        },
        // auto collapse the panel on the first load to simplify the interface,
        // also used by auto-load to determine first load
        firstLoadCollapse: true,
        items: [
            VIEW.Panel.UserSettingsPanel({
                // .settingsPanel.userSettings reference
                ref: 'userSettings'
            }),
            // a container panel and then the feed tabs panel are setup
            // there, .settingsPanel references are established there as well
            VIEW.Panel.FeedSettingsPanel(),
            VIEW.Panel.AuthorSettingsPanel({
                // .settingsPanel.authorSettings reference
                ref: 'authorSettings'
            }),
            VIEW.Panel.TimeSettingsPanel({
                // .settingsPanel.timeSettings reference
                ref: 'timeSettings'
            }),
            VIEW.Panel.AdvancedSettingsPanel({
                // .settingsPanel.advancedSettings reference
                ref: 'advancedSettings'
            })
        ],
        listeners: {
            collapse: function () {
                // ensure collapse occurs on first load only and not
                // on subsequent loads or manual collapses
                this.firstLoadCollapse = false;
            }
        }
    
    }, config));
};
