/**
MASAS View Tool - UserSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file displays a panel with inputs for settings user preferences:
time zone, language, and buttons to save these settings to cookies.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * User settings panel, create & configure using a factory
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.UserSettingsPanel = function (config) {
    var autoLoadCombo = new Ext.form.ComboBox({
        name: 'autoLoad',
        ref: 'autoLoadInterval',
        fieldLabel: 'Auto Load',
        // custom state methods need to be implemented for this combobox
        stateId: 'autoLoad',
        stateful: true,
        getState: function () {
            return { interval: this.getValue() };
        },
        applyState: function (state) {
            if (state.interval) {
                this.setValue(state.interval);
                console.debug('Restored auto-load interval ' + state.interval);
            }
        },
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 100,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        value: 0,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'interval'],
            data: [['Disabled', 0],
                ['5 minutes', 5],
                ['15 minutes', 15],
                ['30 minutes', 30],
                ['1 hour', 60]]
        }),
        listeners: {
            select: function (combo, record, index) {
                console.debug('Auto-load interval ' + record.data.interval);
                if (record.data.interval && record.data.interval !== 0) {
                    Ext.getCmp('autoReloadCheckbox').setValue(true);
                    Ext.getCmp('autoReloadCheckbox').enable();
                } else {
                    // in addition to checking the box should also stop any
                    // running task too
                    Ext.getCmp('autoReloadCheckbox').setValue(false);
                    Ext.getCmp('autoReloadCheckbox').disable();
                }
                // reset this value for the new interval when changed
                VIEW.autoLoadCount = 0;
            },
            afterrender: function (combobox) {
                // if the saved state has autoreload enabled, need to wait until
                // rendering is complete before enabling the checkbox
                if (combobox.getValue() !== 0) {
                    Ext.getCmp('autoReloadCheckbox').setValue(true);
                    Ext.getCmp('autoReloadCheckbox').enable();
                }
            }
        }
    });
    
    return new Ext.FormPanel(Ext.apply({
        title: 'User',
        labelWidth: 70,
        bodyStyle: 'padding: 3px 5px;',
        layout: 'form',
        autoScroll: true,
        items: [{
            xtype: 'compositefield',
            defaults: { flex: 1 },
            // compositefields need to implement their own state methods in
            // order to save and load any values because you can't set a
            // contained element as stateful as the state generator doesn't
            // descend into the compositefield's items
            stateId: 'languageSelect',
            stateful: true,
            getState: function() {
                return {
                    en: Ext.getCmp('languageSelectEN').getValue(),
                    fr: Ext.getCmp('languageSelectFR').getValue()
                };
            },
            applyState: function(state) {
                Ext.getCmp('languageSelectEN').setValue(state.en);
                Ext.getCmp('languageSelectFR').setValue(state.fr);
            },
            items: [{
                xtype: 'radio',
                name: 'languageSelect',
                // accessing via ID and .getCmp() versus ref because of
                // applyState and not all references being ready
                id: 'languageSelectEN',
                fieldLabel: 'Language',
                boxLabel: 'English',
                inputValue: 'en',
                checked: true
            }, {
                xtype: 'radio',
                name: 'languageSelect',
                id: 'languageSelectFR',
                boxLabel: 'French',
                inputValue: 'fr'
            }]
        }, autoLoadCombo, {
            xtype: 'compositefield',
            fieldLabel: 'Map Extent',
            stateId: 'userExtentSaved',
            stateful: true,
            getState: function () {
                return { saved: true };
            },
            // If any settings have been saved, then a saved extent will exist
            // and so we can indicate that here.  Its easier to do that here
            // than in the mapPanel state handlers because they fire before
            // this panel has been created.
            applyState: function (state) {
                if (state.saved) {
                    Ext.getCmp('userExtentSaved').setValue('<span style="color: green;">' +
                        'Saved</span>');
                }
            },
            defaults: { flex: 1 },
            items: [{
                xtype: 'displayfield',
                id: 'userExtentSaved',
                html: '<span style="color: red;">Not Saved</span>'
            }, {
                xtype: 'displayfield',
                style: {
                    'text-align': 'right',
                    'padding-right': '45px'
                },
                html: '<img alt="Help" title="Help" height="18" width="18" src="' +
                    VIEW.ICON_LOCATION + 'info-icon.gif"' +
                    ' onclick="VIEW.show_settings_help()">'
            }]
        }, {
            xtype: 'displayfield',
            hideLabel: true,
            html: '<div style="padding: 10px;"></div>'
        }, {
            // placing the buttons inside the panel instead of in a top or
            // bottom button bar because it highlights them better as they
            // stay directly below the setting values and not way down the
            // screen.  Also prevents mobile users from clicking the accordian
            // titles instead of the button.
            xtype: 'compositefield',
            hideLabel: true,
            defaults: { flex: 1 },
            items: [{
                xtype: 'button',
                text: 'Delete Settings',
                width: 110,
                iconCls: 'btnDelete',
                handler: VIEW.delete_settings
            }, {
                //NOTE: display field is used not only to provide spacing
                //      between the buttons, but also to prevent an error if
                //      only buttons are used in a compositefield without some
                //      other kind of form element included
                xtype: 'displayfield',
                html: ''
            }, {
                xtype: 'button',
                text: 'Save Settings',
                width: 110,
                iconCls: 'btnSave',
                handler: VIEW.save_settings
            }]
        }]
    
    }, config));
};


/**
 * Save settings
 */
VIEW.save_settings = function () {
    Ext.Msg.confirm('Confirm Save',
        'Are you sure that you want to save your new settings?',
        function (confirm) {
            if (confirm === 'yes') {
                console.debug('Saving settings');
                var savestate = function (cmp) {
                    if (cmp.stateful === true) {
                        cmp.saveState();
                    }
                };
                // cascade down all children and save state if stateful
                VIEW.mainView.cascade(savestate);
                // the map toolbars get skipped in the previous cascade
                Ext.each(VIEW.mapPanel.toolbars, function (cmp) {
                    cmp.cascade(savestate);
                });
                // update the extent state field
                Ext.getCmp('userExtentSaved').setValue('<span style="color: green;">' +
                    'Saved</span>');
            }
        }
    );
};


/**
 * Delete settings
 */
VIEW.delete_settings = function () {
    Ext.Msg.confirm('Confirm Delete',
       'Are you sure that you want to permanently delete all saved settings?',
       function (confirm) {
            if (confirm === 'yes') {
                console.debug('Deleting settings');
                var provider = Ext.state.Manager.getProvider();
                var stateIds = Ext.getKeys(provider.state);
                for (var i = 0, len = stateIds.length; i < len; i++) {
                    provider.clear(stateIds[i]);
                }
                // reset the extent state field
                Ext.getCmp('userExtentSaved').setValue('<span style="color: red;">' +
                    'Not Saved</span>');
            }
        }
    );
};


/**
 * Settings help window
 */
VIEW.show_settings_help = function () {
    var win_title = 'Settings Info';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 400,
        height: 300,
        autoScroll: true,
        bodyStyle: {
            'padding': '10px',
            'background-color': 'white'
        },
        html: '<p>&nbsp;&nbsp;There are several settings which can be saved and' +
            ' will be restored when you return.  These settings are saved to' +
            ' cookies in the browser you are currently using.  If you have' +
            ' multiple browsers/devices/computers, you will have to save' +
            ' settings on each of them.</p><br>' +
            '<p>&nbsp;&nbsp;First, select the Language you would like to read' +
            ' content in.  Second, if you want to enable Auto Load, select' +
            ' your desired loading interval.  If you do not want to use Auto' +
            ' Load, leave it Disabled.  Third, configure the Layers toolbar' +
            ' with your desired Base Layers and Overlays.  Finally, pan/zoom' +
            ' the map into your area of interest.  This will ensure that you' +
            ' will only be loading information pertinent to your desired' +
            ' location. </p><br>' +
            '<p>&nbsp;&nbsp;Click Save Settings when you are ready.  You may' +
            ' update your settings at any time by clicking Save Settings again,' +
            ' or delete them entirely using Delete Settings.</p>'
    });
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};
