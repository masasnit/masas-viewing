/**
MASAS View Tool - FeedFeatureStore
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This feature store maps the generic MASAS feed object to a feature store
through an HTTP connection and a feature filter. This object maps feed
elements to feature attributes.  Methods provided to apply user settings
(time zone offset and feed source).
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * Data Feed Feature Store Type
 */
VIEW.FeedStore = Ext.extend(GeoExt.data.FeatureStore, {
    /**
     * Config
     */
    defaultLocal: false,
    
    constructor: function (config) {
        /*
         * Get a map reference so that we can use the map's projection for the
         * format parser just using GeoExt's map panel guess method, configure
         * with a map reference if you want something else
         */
        var map = config.map || GeoExt.MapPanel.guess();
        map = map.map || map;
        Ext.applyIf(config || {}, {
            fields: [{
                name: 'id',
                type: 'string'
            }, {
                name: 'icon',
                type: 'string'
            }, {
                name: 'colour',
                type: 'string'
            }, {
                name: 'title',
                type: 'string'
            }, {
                name: 'author_name',
                type: 'string'
            }, {
                name: 'status',
                type: 'string'
            }, {
                name: 'severity',
                type: 'string'
            }, {
                name: 'category',
                type: 'string'
            }, {
                name: 'published',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'updated',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'effective',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'expires',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'point',
                type: 'string'
            }, {
                name: 'content',
                type: 'string'
            }, 'links', 'linksCombined', {
                name: 'CAP',
                type: 'string'
            }, {
                name: 'source_url',
                type: 'string'
            }, {
                name: 'clusterId',
                type: 'string'
            }],
            batch: true,
            // set any MASAS query parameters that will be common for ALL
            // requests and not dynamically change
            baseParams: {},
            // default sort on first load
            sortInfo: {
                field: 'updated',
                direction: "DESC"
            },
            idProperty: 'id',
            proxy: new GeoExt.data.ProtocolProxy({
                abortPrevious: false,
                protocol: new OpenLayers.Protocol.HTTP({
                    url: (VIEW.AJAX_PROXY_URL) ? VIEW.AJAX_PROXY_URL : undefined,
                    format: new OpenLayers.Format.MASASFeed({
                        internalProjection: map.getProjectionObject(),
                        externalProjection: new OpenLayers.Projection('EPSG:4326')
                    })
                }),
                listeners: {
                    'loadexception': function (proxy, obj, resp) {
                        VIEW.handle_feed_errors(resp.priv);
                    }
                }
            }),
            // filter that runs for every feature added to the store, ensures
            // there are no values missing "updated"
            featureFilter: new OpenLayers.Filter.Comparison({
                type: '!=',
                property: 'updated',
                value: null
            }),
            autoLoad: false,
            // for accessing via Ext.StoreMgr
            storeId: 'FeedStore',
            listeners: {
                'load': this.handleLoad,
                'beforeload': this.clearBySource,
                'datachanged': this.filterFeatures
            }
        });
        VIEW.FeedStore.superclass.constructor.call(this, config);
        
        // add a cluster sync function if the layer has a cluster strategy
        var clusterStrategy = OpenLayers.Array.filter(this.layer.strategies,
            function (s) {
                return s instanceof OpenLayers.Strategy.Cluster;
            })[0];
        if (this.layer && this.layer.strategies && clusterStrategy) {
            this.layer.events.register('featuresadded', this, this.updateClusters);
            this.clusterStrategy = clusterStrategy;                    
        }
    },
    
    
    /**
     * Add a 'clusterId' property to both the store record and the feature so that
     * we can keep track of which cluster the feature/record belongs to
     */
    updateClusters: function (evt) {
        var lyr = this.layer;
        var feats = evt.features;
        // clustering should be finished now
        for (var i = 0, len = feats.length; i < len; i++) {
            var feat = feats[i], clusterId = feat.id;
            for (var j = 0, clen = feat.cluster.length; j < clen; j++) {
                var cfeat = feat.cluster[j];
                var rec = this.getByFeature(cfeat);
                if (rec) {
                    rec.data['clusterId'] = cfeat.attributes['clusterId'] = clusterId;
                }
            }
        }
    },
    
    
    /**
     * Adjust the time offsets for UTC to local time
     */
    applyOffsets: function () {
        //TODO: add an advanced option to allow users to work in the original
        //      UTC values only, skipping this conversion to local time
        var offset = new Date().getTimezoneOffset() / -60;
        if (this.data.length > 0) {
            this.suspendEvents();
            this.each(function (rec) {
                rec.set('published', VIEW.adjust_time(rec.get('published'), offset));
                rec.set('updated', VIEW.adjust_time(rec.get('updated'), offset));
                if (rec.get('effective')) {
                    rec.set('effective', VIEW.adjust_time(rec.get('effective'), offset));
                }
                rec.set('expires', VIEW.adjust_time(rec.get('expires'), offset));
            }, this);
            this.resumeEvents();
            this.commitChanges();
        }
    },
    
    
    /**
     * Set the feed source
     */
    setFeedSource: function (store, records, opts) {
        if (records.length > 0) {
            //var store = feedRecords[0].store
            //store.suspendEvents();
            for (var i = 0, len = records.length; i < len; i++) {
                records[i].set('source_url', opts.sourceId);
            }
            //store.resumeEvents();
            //store.commitChanges();
        }
    },
    
    
    /**
     * Clear Entries by feed source
     */
    clearBySource: function (store, opts) {
        var recs = store.queryBy(function (rec) {
            return rec.data.source_url == opts.sourceId;
        });
        store.remove(recs.items);
        store.commitChanges();
        return true;
    },
    
    
    /**
     * Filter the map features.  The Entry table has already been filtered
     * and now it needs to sync with the map, triggered by the datachanged
     * event which applies to both add/remove and filter/sort of the store.
     *
     * @param {Object} - Feed Store
     */
    filterFeatures: function (store) {
        /*NOTE: When a Feed load occurs, because refilterOnStoreUpdate is
         *      true for the FilterRow extension, this function will be called
         *      multiple times.  Its required because otherwise when the Feed
         *      is reloaded, any existing filtering/sorting won't be applied
         *      to the new Entries.
         */
        if (store.layer) {
            var lyr = store.layer;
            // when there are features to remove from the layer, the
            // clusterStrategy.cacheFeatures is called accordingly, however
            // that triggers the datachanged event for the store, resulting
            // in an in-direct recursive loop.  Prevent this from occuring.
            if (lyr.filterFeaturesRunning) {
                return;
            }
            lyr.filterFeaturesRunning = true;
            var records = store.data;
            // this filtering will happen several times during a Feed load,
            // including when the Feed has been cleared prior to reloading,
            // and for those cases where there are no records nor features,
            // then we can skip the rest
            if (!records.length && !lyr.features.length) {
                lyr.filterFeaturesRunning = false;
                return;
            }
            var toCluster = [];
            var featureIds = [];
            // build the set of records that passed the filter
            store.each(function (rec) {
                var feat = rec.get('feature');
                featureIds.push(feat.id);
                toCluster.push(feat);
            });
            // now modify the features accordingly
            for (var i = 0, len = lyr.features.length; i < len; i++) {
                var feat = lyr.features[i];
                if (feat.cluster) {
                    for (var j = 0, clen = feat.cluster.length; j < clen; j++) {
                        if (featureIds.indexOf(feat.cluster[j].id) > -1) {
                            feat.cluster[j].renderIntent = 'default';
                        } else {
                            feat.cluster[j].renderIntent = 'delete';
                        }
                    }
                } else {
                    if (featureIds.indexOf(feat.id) > -1) {
                        feat.renderIntent = 'default';
                    } else {
                        feat.renderIntent = 'delete';
                    }
                }
            }
            if (this.clusterStrategy  && !this.clusterStrategy.clustering) {
                this.clusterStrategy.features = [];
                this.clusterStrategy.cacheFeatures({features: toCluster});
            } else {
                lyr.redraw();
            }
            
            lyr.filterFeaturesRunning = false;
        }
    },
    
    
    /**
     * After a store load has occured, special handling.  This runs for any
     * load, such as a reset/clear or inputting new Entries.
     *
     * @param {Object} - Feed Store
     * @param {Array} - the new records added
     * @param {Object} - options supplied to the load method
     */
    handleLoad: function (store, records, opts) {
        if (records && records.length) {
            this.applyOffsets();
            this.setFeedSource(store, records, opts);
        }
    }
});

Ext.reg('mv_feedstore', VIEW.FeedStore);
