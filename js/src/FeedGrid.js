/**
MASAS View Tool - FeedGrid
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file implements an Ext GridPanel for the feature store.  Handlers are
provided to filter, sort and select rows and cells.  Also defines the two
buttons for the GridPanel title bar.

@requires src/StoreBoundButton.js
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * Create grid panel configured with feature store
 */
VIEW.FeedGrid = Ext.extend(Ext.grid.GridPanel, {
    /**
     * Config
     */
    expanderTpl: '<p>{content}</p>',
    dateFormat: 'M j H:i:s',
    // row expander
    expander: null,
    featureStore: null,
    layer: null,
    useFilterRow: true,
    // private
    filterStores: null,
    filterRow: null,
    
    /**
     * Initialize
     */
    initComponent: function () {
        this.filterStores = {};
        this.expander = new Ext.ux.grid.RowExpander({
            tpl: new Ext.Template(this.expanderTpl),
            width: 30,
            // instead of a filter combobox, replace with the clear button.
            // See the FilterRow extension for more info
            filter: {
                test: OpenLayers.Function.True,
                field: {
                    xtype: 'button',
                    handler: this.clearFilters,
                    tooltip: 'Clear All Filters',
                    iconCls: 'btnDelete',
                    scope: this
                }
            }
        });
        if (!this.featureStore && this.layer) {
            this.featureStore = new VIEW.FeedStore({
                layer: this.layer
            });
        }
        var fltr = this.useFilterRow;
        if (fltr) {
            this.filterRow = new Ext.ux.grid.FilterRow({refilterOnStoreUpdate: true});
        }
        
        Ext.applyIf(this, {
            title: 'Entries',
            region: 'south',
            // Ext.SplitBar allows the user to click and drag the top of the
            // grid to resize it. Avoid inadvertant use by touch users.
            split: (VIEW.TOUCH_ENABLE) ? false : true,
            height: 225,
            collapsible: false,
            autoScroll: true,
            stripeRows: true,
            columnLines: true,
            header: false,
            store: this.featureStore,
            columns: [this.expander, {
                header: 'Icon',
                width: 37,
                dataIndex: 'colour',
                renderer: this.renderEntryIcon,
                sortable: true,
                // column width shouldn't change
                fixed: true,
                // filter using combobox
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Title',
                id: 'grid_column_title',
                width: 200,
                dataIndex: 'title',
                renderer: this.renderEntryTitle,
                sortable: true,
                // jump to this tab in the contentwindow
                contentTab: 'messages|Atom',
                // filter search bar instead of combobox
                filter: fltr
            }, {
                tooltip: 'Sort by CAP Message Availability',
                width: 22,
                dataIndex: 'links',
                renderer: this.renderCAPIcon,
                sortable: true,
                contentTab: 'messages|CAP',
                fixed: true
            }, {
                tooltip: 'Sort by Attachment Availability',
                width: 20,
                dataIndex: 'links',
                renderer: this.renderAttachmentIcon,
                sortable: true,
                contentTab: 'attachments',
                fixed: true
            }, {
                tooltip: 'Sort by Related Link Availability',
                width: 24,
                dataIndex: 'links',
                renderer: this.renderLinkIcon,
                sortable: true,
                contentTab: 'messages|Atom',
                fixed: true
            }, {
                header: 'Feed',
                width: 60,
                dataIndex: 'source_url',
                renderer: this.renderFeedSource,
                // the feed's name should be used for filtering but since its
                // not available in the record itself, the renderer is used
                // to get it for both display and filtering
                renderFilter: true,
                sortable: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Author',
                width: 125,
                dataIndex: 'author_name',
                sortable: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Status',
                width: 60,
                dataIndex: 'status',
                renderer: this.renderEntryStatus,
                sortable: true,
                fixed: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Severity',
                width: 60,
                dataIndex: 'severity',
                sortable: true,
                fixed: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Certainty',
                width: 60,
                dataIndex: 'certainty',
                sortable: true,
                // default is hidden but user can unhide to customize grid
                //NOTE: when you un-hide a normally hidden field, it can mess
                //      up the filter bar layout.
                hidden: true,
                fixed: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Category',
                width: 100,
                dataIndex: 'category',
                sortable: true,
                fixed: true,
                filter: (fltr) ? this.uniqueValueFilter() : false
            }, {
                header: 'Published',
                width: 90,
                dataIndex: 'published',
                renderer: Ext.util.Format.dateRenderer(this.dateFormat),
                sortable: true,
                hidden: true
            }, {
                header: 'Updated',
                id: 'grid_column_updated',
                width: 90,
                dataIndex: 'updated',
                renderer: this.renderEntryRecent,
                sortable: true
            }, {
                header: 'Effective',
                id: 'grid_column_effective',
                width: 90,
                dataIndex: 'effective',
                renderer: Ext.util.Format.dateRenderer(this.dateFormat),
                sortable: true,
                hidden: true
            }, {
                header: 'Expires',
                width: 90,
                dataIndex: 'expires',
                renderer: this.renderEntryExpires,
                sortable: true
            }, {
                tooltip: 'Zoom to Entry',
                width: 38,
                xtype: 'templatecolumn',
                tpl: '<span class="btnZoomIcon" title="Zoom"></span>',
                dataIndex: 'point',
                sortable: false,
                fixed: true,
                // instead of a filter combobox, replace with the invert button
                filter: {
                    test: OpenLayers.Function.True,
                    field: {
                        xtype: 'button',
                        enableToggle: true,
                        allowDepress: true,
                        pressed: false,
                        iconCls: 'btnInvert',
                        tooltip: 'Invert Filter Selections',
                        handler: this.invertFilters,
                        scope: this
                    }
                }
            }],
            autoExpandColumn: 'grid_column_title',
            // avoids IE issues of having an empty array member
            plugins: (fltr) ? [this.expander, this.filterRow] : [this.expander],
            listeners: {
                'cellmousedown': function (grid, rowIndex, colIndex, evt) {
                    var record = grid.getStore().getAt(rowIndex);
                    var coldef = grid.colModel.getColumnAt(colIndex);
                    var feat = record.getFeature();
                    if (['title', 'links'].indexOf(coldef.dataIndex) > -1) {
                        VIEW.show_entry_content(record.get('links'), record.get('source_url'),
                            coldef.contentTab);
                    } else if (coldef.dataIndex === 'point') {
                        VIEW.zoom_to_feature(record.get('fid'), record.get('source_url'));
                    }
                }
            },
            viewConfig: {
                forceFit: true,
                markDirty: false
            },
            sm: new GeoExt.grid.ClusterSelectionModel(),
            tbar: new Ext.Toolbar({
                // make the toolbar background look like a header
                cls: 'x-panel-header',
                // make the load feed button look like the other load button
                toolbarCls: 'x-panel-fbar',
                height: 30,
                items: [
                    // using a factory method to create this button for both
                    // the grid and map toolbars
                    VIEW.generate_layout_button(),
                    { xtype: 'tbspacer', width: 15 },
                    // give the toolbar a title which looks like a Panel title
                    '<span class="headerBlue">Entries</span>',
                    '<span class="feedUpdateInfo x-hide-display">&nbsp;&nbsp;|&nbsp;&nbsp;</span>',
                    '<span class="feedUpdateInfo x-hide-display">Last Update:&nbsp;</span>',
                    '<span class="feedUpdateInfo x-hide-display" id="feedsUpdateTime"></span>',
                    '<span class="feedUpdateInfo x-hide-display" id="autoReloadTime"></span>',
                    '<span class="feedUpdateInfo x-hide-display">&nbsp;&nbsp;|&nbsp;&nbsp;</span>',
                    '<span class="feedUpdateInfo x-hide-display">Total Entries:&nbsp;</span>',
                    '<span class="feedUpdateInfo x-hide-display" id="entryCount">0</span>',
                    '->',
                    {
                        xtype: 'checkbox',
                        id: 'autoReloadCheckbox',
                        boxLabel: 'Automatic',
                        ctCls: 'autoReloadBox',
                        // disabled until auto-load interval is set
                        disabled: true,
                        handler: function (checkbox, checked) {
                            console.debug('Auto load ' + ((checked) ? 'on' : 'off'));
                            if (checked) {
                                // auto load only starts after the first load
                                // has been done manually, after that this
                                // checkbox will turn on/off
                                if (!VIEW.settingsPanel.firstLoadCollapse &&
                                !VIEW.autoLoadTask) {
                                    VIEW.autoLoadTask = Ext.TaskMgr.start(VIEW.autoLoadConfig);
                                }
                            } else {
                                if (VIEW.autoLoadTask) {
                                    Ext.TaskMgr.stop(VIEW.autoLoadTask);
                                    Ext.getDom('autoReloadTime').innerHTML = '';
                                    VIEW.autoLoadCount = 0;
                                    VIEW.autoLoadTask = null;
                                }
                            }
                        }
                    },
                    { xtype: 'tbspacer', width: 25 },
                    {
                        xtype: 'mv_feedbutton',
                        text: 'Load Feed',
                        iconCls: 'btnRefresh',
                        feedConfigPanel: VIEW.settingsPanel.feedSettings
                    },
                    // this extra space ensures the Load Feed button doesn't
                    // bleed over the edge of the window
                    ' '
                ]
            })
        });
        
        VIEW.FeedGrid.superclass.initComponent.call(this);
        
        // Feed Update display
        this.store.on({
            'load': this.updateFeedDataDisplay,
            'datachanged': this.updateFeedDataDisplay.createDelegate(this),
            scope: this
        });
        
        // don't add these listeners unless we really need to
        if (fltr) {
            this.store.on({
                'load': this.buildUniqueCombos,
                'datachanged': this.buildUniqueCombos.createDelegate(this,
                    [this.store, this.store.data]),
                scope: this
            });
        }
    },
    
    /**
     * Renderer for the Entry icon.
     * NOTE: record.data is the store's field data with the transformations
     * applied by the store, ie date string to Ext date object.  Or
     * record.data.feature.data has the original values.
     */
    renderEntryIcon: function (value, metadata, record) {
        // apply a background colour with the icon
        if (value && metadata) {
            metadata.css = value.toLowerCase() + 'EntryBackground';
        }
        return '<img alt="Event" height="18" src="' + record.data.icon + '">';
    },
    
    /**
     * Content display links accessed via the title
     */
    renderEntryTitle: function (value, metadata, record) {
        // Creating a unique href anchor for each entry based on id and when
        // it was last updated (original UTC time used) to allow the browser to
        // track viewing history for read/unread entries.
        var unique_href = '#' + record.data.feature.data.id + ',' +
            record.data.feature.data.updated;
        var display_link = record.data.linksCombined;
        // read/unread links can be styled accordingly via CSS
        return '<span class="EntryTitleLinks"><a title="' + value + '" href="' +
            unique_href + '">' + value + '</a></span>';
    },
    
    /**
     * Renderer for the CAP Alert icon
     */
    renderCAPIcon: function (value, metadata, record) {
        if (value.CAP) {
            var unique_href = '#' + record.data.feature.data.id + ',' +
                record.data.feature.data.updated;
            return '<a title="Show CAP Message" class="btnAlertIcon" href="' + unique_href + '"></a>';
        }
    },
    
    /**
     * Renderer for the attachment icon
     */
    renderAttachmentIcon: function (value, metadata, record) {
        if (value.attachments) {
            var unique_href = '#' + record.data.feature.data.id + ',' +
                record.data.feature.data.updated;
            return '<a title="Show Attachment" class="btnAttachIcon" href="' + unique_href +
                '"></a>';
        }
    },
    
    /**
     * Renderer for the link icon
     */
    renderLinkIcon: function (value, metadata, record) {
        if (value.xlink) {
            var unique_href = '#' + record.data.feature.data.id + ',' +
                record.data.feature.data.updated;
            return '<a title="Show Related Link" class="btnLinkIcon" href="' + unique_href +
                '"></a>';
        }
    },
    
    /**
     * Renderer for the feed source.  The grid record uses the feed's source_url
     * to ensure uniqueness but for user display the feed's name is used both
     * in the grid and as the modified value for filtering
     */
    renderFeedSource: function (value, metaData, record, rowIndex, colIndex, store) {
        var sourceRecord = Ext.StoreMgr.get('FeedSources').getById(value);
        return (sourceRecord) ? sourceRecord.get('title') : value;
    },
    
    /**
     * Renderer for Status values, highlights exceptional values
     */
    renderEntryStatus: function (value, metadata) {
        if (value === 'Test' || value === 'Draft') {
            metadata.css = 'EntryStatusException';
        }
        return value;
    },
    
    /**
     * Highlight recent entries
     */
    renderEntryRecent: function (value, metadata, record) {
        if (!value) {
            value = record.data[this.renderIndex];
        }
        var updated_epoch = value.format('U') * 1000;
        var recent_epoch = new Date().getTime();
        // 15 minutes ago or less is "recent" and will be highlighted
        recent_epoch -= 900000;
        if (updated_epoch > recent_epoch) {
            metadata.css = 'EntryRecentTime';
        }
        return value.format('M j H:i:s');
    },
    
    /**
     * Highlight expired entries
     */
    renderEntryExpires: function (value, metadata, record) {
        if (!value) {
            value = record.data[this.renderIndex];
        }
        var expires_epoch = value.format('U') * 1000;
        var now_epoch = new Date().getTime();
        if (expires_epoch <= now_epoch) {
            metadata.css = 'EntryExpiredTime';
        }
        return value.format('M j H:i:s');
    },
    
    /**
     * Factory that creates the filter object for the FilterRow plugin,
     * populating with a combo, some configs, and the filter testing function
     */
    uniqueValueFilter: function () {
        var combo = new Ext.form.ComboBox({
            store: ['-'],
            value: '-',
            forceSelection: true,
            autoSelect: true,
            lazyRender: true,
            triggerAction: 'all',
            // by not allowing someone to type a value in here, it makes selection
            // on a tablet device much easier
            editable: false
        });
        // don't auto-expand the dropdown when unique values are added each
        // time the feed reloads
        combo.store.on('load', function () {
            this.collapse();
        }, combo, {
            delay: 150
        });
        
        return {
            field: combo,
            showFilterIcon: false,
            fieldEvents: ['select'],
            test: function (filterValue, value, record, renderer) {
                if (renderer) {
                    value = renderer(value, {}, record);
                }
                return filterValue === "-" || filterValue === value;
            },
            uniqueValues: true,
            scope: this
        };
    },
    
    /**
     * Load the unique values into the filtering combo boxes
     */
    buildUniqueCombos: function (store, records) {
        if (records && records.length && this.filterRow) {
            this.filterRow.eachFilterColumn(function (col, ndx) {
                if (col.filter.uniqueValues) {
                    var uniqueValues = store.collect(col.dataIndex, false, true);
                    // if the record doesn't contain the value that should be
                    // used for filtering, use the renderer to generate and
                    // replace instead
                    if (col.renderFilter) {
                        Ext.each(uniqueValues, function (val, ndx, all) {
                            uniqueValues[ndx] = col.renderer(val);
                        });
                        // change the filter to use this renderer instead,
                        // but should need to run only once on initial setup
                        if (!col.filter.modifyValues) {
                            col.filter.modifyValues = true;
                            col.filter.test = col.filter.test.createDelegate(this,
                                [col.renderer], true);
                        }
                    }
                    // adding the default value for "All"
                    uniqueValues.splice(0, 0, '-');
                    col.filter.field.store.loadData(uniqueValues);
                }
            });
        }
    },
    
    /**
     * Clear the filters
     */
    clearFilters: function () {
        // first reset the layer filtering flag in case there has been any
        // error leaving it in the wrong state, so that the next set of changes
        // can take place
        this.store.layer.filterFeaturesRunning = false;
        // reset all columns back to the default "-" value
        this.filterRow.eachFilterColumn(function (col, i) {
            var fld = col.filter.field;
            // make sure it is actually a resetable field and has a non-null,
            // non-default value
            if (fld.reset) {
                var oldVal = fld.getValue();
                fld.reset();
                // only fire change event if you need to, 'cause it's kind of expensive
                if (oldVal && oldVal !== '-') {
                    col.filter.fireEvent('change');
                }
            }
        });
    },
    
    /**
     * Invert the filters
     */
    invertFilters: function (btn, evt) {
        var pressed = btn.pressed;
        var normalTest = function (filterValue, value, record, renderer) {
            if (renderer) {
                value = renderer(value, {}, record);
            }
            return filterValue === "-" || filterValue === value;
        };
        var invertTest = function (filterValue, value, record, renderer) {
            if (renderer) {
                value = renderer(value, {}, record);
            }
            return filterValue === "-" || filterValue !== value;
        };
        this.filterRow.eachFilterColumn(function (col, i) {
            var filter = col.filter;
            if (filter.field instanceof Ext.form.ComboBox && filter.test) {
                filter.test = (pressed) ? invertTest : normalTest;
                if (filter.field.getValue() !== '-') {
                    filter.fireEvent('change');
                }
            }
        });
    },
    
    /**
     * Update the feed data display with count and time
     */
    updateFeedDataDisplay: function (store, records) {
        Ext.select('.feedUpdateInfo', true).removeClass('x-hide-display');
        if (records) {
            // only present for load event
            Ext.getDom('feedsUpdateTime').innerHTML = new Date().format(this.dateFormat);
        }
        Ext.getDom('entryCount').innerHTML = store.getCount();
    }
    
});

Ext.reg('mv_feedgrid', VIEW.FeedGrid);


/**
 * Load feed button, extends the custom StoreBoundButton adding initComponent,
 * and custom methods.  Defines a combo button to load all the feeds or load feeds
 * individually.  The list of feeds comes from the FeedSources store.
 */
VIEW.LoadFeedButton = Ext.extend(Ext.StoreBoundButton, {
    /**
     * Config
     */
    feedConfigPanel: null,
    
    /**
     * Initialize
     */
    initComponent: function () {
        var config = Ext.applyIf(this.initialConfig, {
            itemBinding: 'title',
            itemHandler: this.loadSingleFeed.createDelegate(this),
            handler: this.loadAllFeeds,
            store: Ext.StoreMgr.get('FeedSources')
        });
        Ext.apply(this, config);
        VIEW.LoadFeedButton.superclass.initComponent.call(this);
        this.menu.on({
            'beforeshow': function (menu) {
                if (this.feedConfigPanel) {
                    this.feedConfigPanel.syncRecordsToForm();
                }
            },
            scope: this
        });
    },
    
    /**
     * Load a single feed
     */
    loadSingleFeed: function (item, evt) {
        VIEW.load_single_feed(this.store, item.recordId);
    },
    
    /**
     * Load all feeds
     */
    loadAllFeeds: function (btn, evt) {
        this.feedConfigPanel.syncRecordsToForm();
        VIEW.load_all_feeds(this.store);
        // auto collapse the user panel on the first load to simplify the interface
        // doesn't collapse if a single feed is loaded or if its already been
        // collapsed before
        if (VIEW.settingsPanel.firstLoadCollapse) {
            VIEW.settingsPanel.collapse();
        }
        // auto load enabled
        if (Ext.getCmp('autoReloadCheckbox').getValue()) {
            if (!VIEW.autoLoadTask) {
                // usually on first startup, user must manually load first to
                // start the auto reload timer
                VIEW.autoLoadTask = Ext.TaskMgr.start(VIEW.autoLoadConfig);
            } else {
                // otherwise if they manually reload, then reset the auto load
                // interval so it starts from this most recent manual load
                var load_interval = VIEW.settingsPanel.userSettings.autoLoadInterval.getValue();
                Ext.getDom('autoReloadTime').innerHTML = '&nbsp;&nbsp;|&nbsp;&nbsp;' +
                    'Auto Load in ' + load_interval + ' minutes';
                VIEW.autoLoadCount = 0;
            }
        }
    },
    
    /**
     * Update a feed based on store changes
     */
    onStoreItemUpdate: function (store, record, op) {
        var item = VIEW.LoadFeedButton.superclass.onStoreItemUpdate.apply(this, arguments);
        item.setDisabled(record.get('disabled'));
    }
});

Ext.reg('mv_feedbutton', VIEW.LoadFeedButton);


/**
 * A factory method used for the layout buttons on the grid and map toolbars.
 */
VIEW.generate_layout_button = function (map) {
    return {
        id: (map) ? 'layoutButtonMap' : 'layoutButtonGrid',
        iconCls: 'btnGridControl',
        tooltip: 'Change the Window Layout',
        width: 40,
        // the map toolbar button is normally hidden, except when in Map Only
        hidden: (map) ? true : false,
        // don't allow the toolbar to reorder this button's location
        reorderable: false,
        menu: {
            xtype: 'menu',
            plain: true,
            defaults: {
                style: { margin: '5px 0px' }
            },
            items: [{
                text: 'Map Only',
                handler: function () {
                    console.debug('Map only layout');
                    VIEW.settingsPanel.hide();
                    VIEW.mapPanel.show();
                    // show on the map toolbar so its easy for the user to click
                    // rather than the very bottom corner
                    var map_btn = Ext.getCmp('layoutButtonMap');
                    map_btn.show();
                    var grid_btn = Ext.getCmp('layoutButtonGrid');
                    grid_btn.hide();
                    // the height of the tbar set in FeedGrid
                    VIEW.gridPanel.setHeight(30);
                    // repositions all of the border layout items because only
                    // the Grid has a defined height.
                    VIEW.mainView.doLayout();
                }
            }, {
                text: 'Map and Grid',
                handler: function () {
                    console.debug('Map and grid layout');
                    VIEW.settingsPanel.show();
                    VIEW.mapPanel.show();
                    var map_btn = Ext.getCmp('layoutButtonMap');
                    map_btn.hide();
                    var grid_btn = Ext.getCmp('layoutButtonGrid');
                    grid_btn.show();
                    // match the default defined in FeedGrid
                    VIEW.gridPanel.setHeight(225);
                    VIEW.mainView.doLayout();
                }
            }, {
                text: 'Grid Only',
                handler: function () {
                    console.debug('Grid only layout');
                    var map_btn = Ext.getCmp('layoutButtonMap');
                    map_btn.hide();
                    var grid_btn = Ext.getCmp('layoutButtonGrid');
                    grid_btn.show();
                    var view_size = VIEW.mainView.getSize();
                    // defend against invalid/undefined heights
                    if (view_size && view_size.height) {
                        VIEW.gridPanel.setHeight(view_size.height);
                        VIEW.settingsPanel.hide();
                        VIEW.mapPanel.hide();
                        VIEW.mainView.doLayout();
                    }
                }
            }]
        }
    };
};
