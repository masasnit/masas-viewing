/**
MASAS View Tool - AdvancedSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file displays a panel with checkboxes for global options on the app, 
including the option to show/hide detailed popups, to automatically
adjust icon positions when the centroid is outside the viewport,
to hide all the icons, and to show all original entry geometries.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * Advanced settings panel, create & configure using a factory
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.AdvancedSettingsPanel = function (config) {
    return new Ext.FormPanel(Ext.apply({
        title: 'Advanced',
        layout: 'form',
        autoScroll: true,
        labelWidth: 10,
        bodyStyle: 'padding: 10px;',
        cls: 'leftAlign',
        defaults: {stateful: false},
        //TODO: checkboxes don't offer ExtJS based tooltips.  Find a better way
        //      to show them compared to the span/title methods below.
        items: [{
            boxLabel: '<span title="Toggle between simple and detailed information popups">Detailed Popups</span>',
            xtype: 'checkbox',
            checked: false,
            ref: 'detailedPopups',
            handler: function (cmp, checked) {
                console.debug('Detailed Popups ' + ((checked) ? 'on' : 'off'));
                if (checked) {
                    // indicate this advanced option is selected
                    cmp.ownerCt.setTitle('<b>Advanced</b>');
                } else {
                    cmp.ownerCt.setTitle('Advanced');
                }
            }
        }, {
            boxLabel: '<span title="Adjusts icon positions so they remain visible">Auto Adjust Entry Icons</span>',
            xtype: 'checkbox',
            checked: true,
            ref: 'adjustIcons',
            handler: function (cmp, checked) {
                console.debug('Adjust Icons ' + ((checked) ? 'on' : 'off'));
                //NOTE: in VIEW. namespace to allow MASAS.js to access
                VIEW.adjustIcons = checked;
                if (!checked) {
                    cmp.ownerCt.setTitle('<b>Advanced</b>');
                } else {
                    cmp.ownerCt.setTitle('Advanced');
                }
            }
        }, {
            boxLabel: '<span title="Hide all icons">Hide Entry Icons</span>',
            xtype: 'checkbox',
            checked: false,
            ref: 'hideIcons',
            handler: function (cmp, checked) {
                console.debug('Hide Icons ' + ((checked) ? 'on' : 'off'));
                VIEW.mapLayers.MASASLayer.setVisibility((checked) ? false : true);
                if (checked) {
                    cmp.ownerCt.setTitle('<b>Advanced</b>');
                } else {
                    cmp.ownerCt.setTitle('Advanced');
                }
            }
        }, {
            xtype: 'compositefield',
            // compositefields need to implement their own state methods in
            // order to save and load any values
            stateId: 'entryDisplayLimit',
            stateful: true,
            getState: function() {
                return { selected: Ext.getCmp('entryDisplayLimit').getValue() };
            },
            applyState: function(state) {
                if (state.selected) {
                    // not 'select'ing the value as that would fire the
                    // select event when it shouldn't
                    Ext.getCmp('entryDisplayLimit').value = state.selected;
                    console.debug('Restored entry display limit ' + state.selected);
                }
            },
            items: [{
                xtype: 'displayfield',
                html: '<span title="Set the maximum number of entries that can be displayed">Entry Display Limit </span>'
            }, {
                xtype: 'combo',
                name: 'entryDisplayLimit',
                // accessing via ID and .getCmp() versus ref because of
                // applyState and not all references being ready
                id: 'entryDisplayLimit',
                width: 60,
                store: [100, 200, 400, 600, 800, 1000],
                // use lowest limit for mobile devices to keep them responsive,
                // IE 8 performance dictates the desktop default of 200
                value: (VIEW.TOUCH_ENABLE) ? 100 : 200,
                // by not allowing someone to type a value in here, it makes
                // selection on a tablet device much easier
                editable: false,
                forceSelection: true,
                triggerAction: 'all',
                listeners: {
                    select: function (cmp, record) {
                        console.debug('Entry display limit changed');
                        alert('NOTE: Changing the Entry Display Limit can affect ' +
                            'your web browser performance with higher limits ' +
                        'potentially causing sluggish behaviour.');
                    }
                }
            }]
        }]
    
    }, config));
};
