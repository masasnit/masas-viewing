/**
MASAS View Tool - TimeSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file displays a form panel for specifying a feed time filter to restrict
the range of valid dates for entries returned by a feed.  These values should
be in Local time and will be converted to UTC when loading the feed.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * Date and time settings panel, create & configure using a factory
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.TimeSettingsPanel = function (config) {
    var searchTypeCombo = new Ext.form.ComboBox({
        name: 'searchType',
        ref: '../searchType',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 85,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'name',
        value: 'updated',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'tip'],
            data: [['updated', 'Search for updated during this period'],
                ['published', 'Search for initially published during this period'],
                ['effective', 'Search for becoming effective during this period'],
                ['expires', 'Search for having expired during this period']]
        }),
        listeners: {
            change: function (field, newValue, oldValue) {
                var grid_columns = VIEW.gridPanel.getColumnModel();
                var updated_column = grid_columns.getIndexById('grid_column_updated');
                var effective_column = grid_columns.getIndexById('grid_column_effective');
                if (newValue === 'effective') {
                    // show the effective column instead when users are
                    // viewing future entries
                    //TODO: the Title in the column seems to expand in size
                    //      pushing some items around, need to look into how
                    //      to prevent this
                    grid_columns.setHidden(updated_column, true);
                    grid_columns.setHidden(effective_column, false);
                } else {
                    if (oldValue === 'effective') {
                        // if effective was previously used, reset back to
                        // updated as default
                        grid_columns.setHidden(updated_column, false);
                        grid_columns.setHidden(effective_column, true);
                    }
                }
            }
        }
    });
    
    return new Ext.FormPanel(Ext.apply({
        title: 'Time',
        layout: 'form',
        autoScroll: true,
        // not using labels but this does add some indenting underneath the
        // radio boxes
        labelWidth: 15,
        bodyStyle: 'padding: 10px;',
        defaults: { msgTarget: 'side' },
        items: [{
            xtype: 'radio',
            name: 'timeSelect',
            hideLabel: true,
            boxLabel: '<b>Current</b>',
            inputValue: 'current',
            checked: true,
            handler: function (cmp, checked) {
                if (!checked) {
                    // when not using current, disable auto reload.  The user
                    // can turn it back on if needed
                    Ext.getCmp('autoReloadCheckbox').setValue(false);
                    // indicate that time filters are in use
                    cmp.ownerCt.setTitle('<b>Time</b>');
                } else {
                    cmp.ownerCt.setTitle('Time');
                }
            }
        }, {
            xtype: 'displayfield',
            html: '<hr>'
        }, {
            xtype: 'radio',
            name: 'timeSelect',
            hideLabel: true,
            boxLabel: 'Since',
            inputValue: 'since'
        }, {
            xtype: 'compositefield',
            defaults: { flex: 1 },
            items: [{
                xtype: 'datefield',
                name: 'sinceDT',
                width: 95,
                format: 'Y-m-d',
                // starts out with today's date and 00:00 for time
                value: new Date(),
                // in a composite field so move up to attach the reference
                // to the actual form panel
                ref: '../sinceDT'
            }, {
                xtype: 'timefield',
                name: 'sinceTM',
                width: 60,
                format: 'H:i',
                increment: 60,
                value: '00:00',
                ref: '../sinceTM'
            }]
        }, {
            xtype: 'displayfield',
            html: '<hr>'
        }, {
            xtype: 'radio',
            name: 'timeSelect',
            hideLabel: true,
            boxLabel: 'Range',
            inputValue: 'range'
        }, {
            xtype: 'compositefield',
            defaults: { flex: 1 },
            items: [{
                xtype: 'displayfield',
                width: 35,
                html: 'Start:'
            }, {
                xtype: 'datefield',
                name: 'startDT',
                id: 'startDT',
                width: 95,
                format: 'Y-m-d',
                vtype: 'daterange',
                // ID of the end date field used for validation
                endDateField: 'endDT',
                ref: '../startDT'
            }, {
                xtype: 'timefield',
                name: 'startTM',
                width: 60,
                format: 'H:i',
                increment: 60,
                value: '00:00',
                ref: '../startTM'
            }]
        }, {
            xtype: 'compositefield',
            defaults: { flex: 1 },
            items: [{
                xtype: 'displayfield',
                width: 35,
                html: 'End:'
            }, {
                xtype: 'datefield',
                name: 'endDT',
                id: 'endDT',
                width: 95,
                format: 'Y-m-d',
                vtype: 'daterange',
                // ID of the start date field used for validation
                startDateField: 'startDT',
                ref: '../endDT'
            }, {
                xtype: 'timefield',
                name: 'endTM',
                width: 60,
                format: 'H:i',
                increment: 60,
                value: '00:00',
                ref: '../endTM'
            }]
        }, {
            xtype: 'compositefield',
            defaults: { flex: 1 },
            items: [{
                xtype: 'displayfield',
                width: 35,
                html: 'Value:'
            }, searchTypeCombo]
        }],
        //TODO: consider whether to move these into the panel itself like
        //      the user settings buttons
        tbar: [{
            text: 'Previous 24 hrs',
            iconCls: 'x-tbar-page-prev',
            handler: function (btn) {
                // use Since with a value that is 24 hours in the past
                var time_panel = btn.ownerCt.ownerCt;
                var time_form = time_panel.getForm();
                time_form.findField('timeSelect').setValue('since');
                var past = VIEW.adjust_time(new Date(), -24);
                time_panel.sinceDT.setValue(past.format('Y-m-d'));
                time_panel.sinceTM.setValue(past.format('H:i'));
                // set the value to the default and fire change event
                time_panel.searchType.setValue('updated');
                time_panel.searchType.fireEvent('change', time_panel.searchType,
                    'updated', 'effective');
            }
        }, '->', {
            text: 'Future 24 hrs',
            iconCls: 'x-tbar-page-next',
            iconAlign: 'right',
            handler: function (btn) {
                // use Range with a start value of now and end value 24 hours
                // in the future, using effective for the search type
                var time_panel = btn.ownerCt.ownerCt;
                var time_form = time_panel.getForm();
                time_form.findField('timeSelect').setValue('range');
                var now = new Date();
                var future = VIEW.adjust_time(now, 24);
                time_panel.startDT.setValue(now.format('Y-m-d'));
                time_panel.startTM.setValue(now.format('H:i'));
                time_panel.endDT.setValue(future.format('Y-m-d'));
                time_panel.endTM.setValue(future.format('H:i'));
                // set the new value and fire change event
                time_panel.searchType.setValue('effective');
                time_panel.searchType.fireEvent('change', time_panel.searchType,
                    'effective', 'updated');
            }
        }]
        
    }, config));
};
