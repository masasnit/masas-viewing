/**
MASAS View Tool - AuthorSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

A panel added to Settings that allows a user to select a list of Authors
and either Hide or Show them when loading a feed.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * Author settings panel, create & configure using a factory
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.AuthorSettingsPanel = function (config) {
    var list_panel = new Ext.Panel({
        flex: 1,
        autoScroll: true,
        hideLabel: true,
        items: new Ext.DataView({
            ref: '../authorList',
            tpl: VIEW.Template.Author_List_Template,
            store: new Ext.data.ArrayStore({
                // remove directs the template to show a delete icon instead
                fields: ['uri', 'name', 'org_name', 'remove'],
                idIndex: 0,
                data: []
            }),
            // this sets the div that will fire the click event
            itemSelector: 'div.authorListRemove',
            singleSelect: true,
            loadingText: 'Loading...',
            listeners: {
                click: function (dataview, index, node) {
                    console.debug('Removing author from list');
                    dataview.store.removeAt(index);
                    dataview.refresh();
                }
            }
        }),
        tbar: ['->', {
            xtype: 'button',
            text: 'Add',
            tooltip: 'Add Authors to this list',
            iconCls: 'btnAddIcon',
            handler: VIEW.author_search_window
        }]
    });
    
    return new Ext.FormPanel(Ext.apply({
        title: 'Authors',
        bodyStyle: 'padding: 5px;',
        layout: 'form',
        autoScroll: true,
        items: [{
            xtype: 'buttongroup',
            ref: 'authorFilter',
            columns: 3,
            hideLabel: true,
            frame: false,
            buttonAlign: 'center',
            defaults: {
                enableToggle: true,
                toggleGroup: 'authorF',
                style: { 'padding-right': '15px' },
                width: 85
            },
            // used to determine if filtering is on/off
            filterSetting: false,
            items: [{
                text: 'All',
                tooltip: 'Show All Authors',
                pressed: true,
                toggleHandler: function (btn, state) {
                    if (state) {
                        btn.ownerCt.filterSetting = false;
                        btn.ownerCt.ownerCt.setTitle('Authors');
                        console.debug('Author filter changed to all');
                    } else {
                        // indicate that author filters are in use
                        btn.ownerCt.ownerCt.setTitle('<b>Authors</b>');
                    }
                }
            }, {
                text: 'Hide',
                tooltip: 'Hide any Authors from this list',
                toggleHandler: function (btn, state) {
                    if (state) {
                        btn.ownerCt.filterSetting = 'hide';
                        console.debug('Author filter changed to hide');
                    }
                }
            }, {
                text: 'Show',
                tooltip: 'Only Show Authors from this list',
                toggleHandler: function (btn, state) {
                    if (state) {
                        btn.ownerCt.filterSetting = 'show';
                        console.debug('Author filter changed to show');
                    }
                }
            }]
        }, list_panel
        ]
    
    }, config));
};


/**
 * A window that allows the user to either search for Authors to add to the
 * list or select from some saved defaults.
 */
VIEW.author_search_window = function () {
    var saved_panel = new Ext.Panel({
        title: 'Saved',
        flex: 1,
        autoScroll: true,
        items: new Ext.DataView({
            tpl: VIEW.Template.Author_List_Template,
            store: new Ext.data.ArrayStore({
                fields: ['uri', 'name', 'org_name'],
                idIndex: 0,
                data: []
            }),
            // this sets the div that will fire the click event
            itemSelector: 'div.authorListAdd',
            singleSelect: true,
            loadingText: 'Loading...',
            listeners: {
                click: function (dataview, index) {
                    var author_data = dataview.store.getAt(index).data;
                    author_data['remove'] = true;
                    var list_store = VIEW.settingsPanel.authorSettings.authorList.store;
                    list_store.add(new list_store.recordType(author_data));
                    VIEW.settingsPanel.authorSettings.authorList.refresh();
                    console.debug('Added ' + author_data.name + ' to author list');
                },
                render: function (dataview) {
                    if (VIEW.SAVED_AUTHORS) {
                        dataview.store.loadData(VIEW.SAVED_AUTHORS);
                    }
                }
            }
        })
    });
    
    var search_store = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: VIEW.ACCESS_CONTROL_SEARCH_URL
        }),
        reader: new Ext.data.JsonReader({
            fields: [
                {name: 'name'},
                {name: 'uri'},
                {name: 'org_name'}
            ]
        }),
        listeners: {
            exception: function (proxy, type, action, options, response) {
                // clears the loadingText in DataView
                this.removeAll();
                var error_msg = type;
                if (type === 'response') {
                    // if identified as a server error, add its response
                    if (response && response.responseText) {
                        error_msg += ' - ' + response.responseText;
                    }
                }
                console.error('Author search error: ' + error_msg);
                Ext.Msg.alert('Search Error', 'An error occurred with your' +
                    ' search, please try again.');
            }
        }
    });
    
    var search_panel = new Ext.Panel({
        title:'Search',
        flex: 1,
        autoScroll:true,
        items: new Ext.DataView({
            tpl: VIEW.Template.Author_List_Template,
            store: search_store,
            // this sets the div that will fire the click event
            itemSelector: 'div.authorListAdd',
            singleSelect: true,
            loadingText: 'Searching...',
            listeners: {
                click: function (dataview, index) {
                    var author_data = dataview.store.getAt(index).data;
                    author_data['remove'] = true;
                    var list_store = VIEW.settingsPanel.authorSettings.authorList.store;
                    list_store.add(new list_store.recordType(author_data));
                    VIEW.settingsPanel.authorSettings.authorList.refresh();
                    console.debug('Added ' + author_data.name + ' to author list');
                }
            }
        }),
        tbar: [{
            xtype: 'combo',
            id: 'authorSearchType',
            width: 90,
            // by not allowing someone to type a value in here, it makes
            // selection on a tablet device much easier
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            value: 'Name',
            store: ['Name', 'Organization', 'Contact']
        }, {
            xtype: 'tbspacer',
            width: 15
        }, 'Search:',
            // custom search field
            new VIEW.Panel.AuthorSearchField({
                store: search_store
            })
        ]
    });
    
    var win_title = 'Add Author';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 400,
        height: 400,
        autoScroll: true,
        items: [{
            xtype: 'tabpanel',
            activeTab: 0,
            items: [ saved_panel, search_panel ]
        }]
    });
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Extends from Triggerfield to add a textfield with a search icon
 */
VIEW.Panel.AuthorSearchField = Ext.extend(Ext.form.TriggerField, {
    /**
     * Config defaults, may be overriden
     */
    validationEvent: false,
    validateOnBlur: false,
    triggerClass: 'x-form-search-trigger',
    width: 200,
    
    /**
     * Initialize
     */
    initComponent : function () {
        Ext.form.TriggerField.superclass.initComponent.call(this);
        this.on('specialkey', function (f, e) {
            // search on Enter as well as search icon click
            if (e.getKey() == e.ENTER) {
                this.onTriggerClick();
            }
        }, this);
    },
    
    /**
     * Search icon
     */
    onTriggerClick : function () {
        // the entered value
        var v = this.getRawValue();
        if (v.length < 3) {
            // clear
            this.store.removeAll();
            return;
        }
        
        // using the first secret value for access control
        var first_feed = Ext.StoreMgr.get('FeedSources').getAt(0);
        var first_secret = first_feed.get('secret');
        // search type combobox
        var search_type = Ext.getCmp('authorSearchType').getValue().toLowerCase();
        if (search_type === 'organization') {
            search_type = 'org';
        }
        // build the search url, urlAppend adds the correct ? or &
        var search_url = Ext.urlAppend(VIEW.ACCESS_CONTROL_SEARCH_URL,
            'secret=' + first_secret);
        search_url += '&' + search_type + '=' + v;
        // support proxies
        if (VIEW.AJAX_PROXY_URL) {
            //NOTE: differences between VIEW and POST proxy URLs
            search_url = Ext.urlAppend(VIEW.AJAX_PROXY_URL, 'url=') +
                encodeURIComponent(search_url);
        }
        
        this.store.proxy.setUrl(search_url);
        this.store.load();
    }
});
