/**
MASAS View Tool - FeedSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines the panel, store, and form for specifying all the details about
a feed, including url, secret and name.  Each feed is available as a tab in 
the panel along with an 'add feed' tab.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * Feed settings panel, create & configure using a factory, is a frame to
 * hold the tab panel which does all the work.
 *
 * @param {Object} - config values to be added or to overwrite any defaults
 */
VIEW.Panel.FeedSettingsPanel = function (config) {
    return new Ext.Panel(Ext.apply({
        title: 'Feeds',
        layout: 'fit',
        autoScroll: true,
        items: [{
            xtype: 'mv_feedsettings',
            // the tab panel will be the settingsPanel.feedSettings reference
            ref: '../feedSettings'
        }]
    
    }, config));
};


/**
 * A custom store for the feed sources, primarily accessed after being
 * established via the constructor through Ext.StoreMgr(storeId)
 */
VIEW.FeedSourceStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (config) {
        config = config || {};
        var fields = [
        // required settings in VIEW.FEED_SETTINGS
        'title', 'url', 'secret', {
            // optional setting to render this feed disabled at load
            name: 'disabled',
            type: 'boolean',
            defaultValue: false
        }, {
            // optional setting to hide the user's secret value at load
            name: 'hideSecret',
            type: 'boolean',
            defaultValue: true
        }, {
            // optional setting to prevent the user from changing this feed
            name: 'locked',
            type: 'boolean',
            defaultValue: false
        }];
        config.fields = (config.fields instanceof Array) ? config.fields.concat(fields) : fields;
        Ext.apply(config, {
            storeId: 'FeedSources',
            idProperty: 'url'
        });
        VIEW.FeedSourceStore.superclass.constructor.call(this, config);
    }
});


/**
 * Feed settings tab panel, extends tabpanel adding a constructor, initComponent,
 * and custom methods.
 */
VIEW.Panel.FeedSettingsTabPanel = Ext.extend(Ext.TabPanel, {
    /**
     * Config
     */
    sourceStore: null,
    sources: null,
    // don't automatically hide the secret when creating a new feed, allows
    // the user to see what they are typing
    hideSecrets: false,
    // max number of feed tabs, current set a 5 because any more and they fill
    // up the available space in the tab header
    //TODO: find out how to expand the tab header to add a second row of feeds
    //      that would allow increasing this max count
    maxFeedCount: 5,
    
    constructor: function (config) {
        config = config || {};
        // if no store is configured then look for the store in the store manager
        if (!config.sourceStore && Ext.StoreMgr.get('FeedSources')) {
            config.sourceStore = Ext.StoreMgr.get('FeedSources');
        }
        // if no store is found in manager or configured then add a new one
        else if (!config.sourceStore) {
            config.sourceStore = new VIEW.FeedSourceStore();
        }
        else if (!(config.sourceStore instanceof Ext.data.Store)) {
            config.sourceStore = new VIEW.FeedSourceStore(config.sourceStore);    
        }
        
        config.listeners = config.listeners || {};
        Ext.applyIf(config.listeners, {
            // add listeners config here
            'tabchange': this.syncRecordsToForm,
            'beforetabchange': this.handleAddTab,
            scope: this
        });
        
        VIEW.Panel.FeedSettingsTabPanel.superclass.constructor.call(this, config);
    },
    
    /**
     * Initialize
     */
    initComponent: function () {
        // determines whether each feed will save its state in buildForm and
        // also determines whether those state cookies will be loaded and used.
        // MASAS-X is currently not using.
        this.stateful = false;
        // load the source configs into the source store if available
        if (this.initialConfig.sources) {
            this.sourceStore.loadData(this.initialConfig.sources);
        }
        
        // tab panel defaults
        Ext.applyIf(this, {
            enableTabScroll: true,
            plain: true,
            layoutOnTabChange: true,
            // render right away in order to populate the tabs properly
            deferredRender: false,
            // state ID used in state cookies
            stateId: 'feeds'
        });
        
        VIEW.Panel.FeedSettingsTabPanel.superclass.initComponent.call(this);
        
        // add the feed source panels if the source store contains items
        if (this.sourceStore.data.length) {
            this.sourceStore.each(this.addByRecord, this);
            this.setActiveTab(0);
        }
        
        // the add new feed tab
        this.add({
            layout: 'fit',
            // + sign
            title: '&#43;',
            tabTip: 'Add a new feed source',
            ref: 'addTab',
            xtype: 'panel',
            // according to http://www.sencha.com/forum/showthread.php?81087-OPEN-418-3.0.0-el.ownerDocument.createRange%28%29-error-in-IE8
            // use a span instead of &nbsp;
            html: '<span></span>'
        });
    },
    
    /**
     * Sync records to the forms on the panel
     */
    syncRecordsToForm: function (index) {
        if (!index && index !== 0) {
            if (this.sourceStore.getCount() == 0 && this.sourceStore.isFiltered() &&
            this.items.length <= this.sourceStore.totalLength) {
                // all feeds are disabled. do nothing and stop processing
                return false;
            } else {
                // sync all
                this.items.each(function (item, index, len) {
                    this.syncRecordsToForm(index);
                }, this);
            }
        } else {
            // sync the indexed form
            var form = this.items.itemAt(index);
            if (form instanceof Ext.form.FormPanel) {
                var record = this.sourceStore.getAt(index);
                var bform = form.getForm();
                // force form validation. don't save or update records with invalid data
                if (!bform.isValid()) {
                    /*
                    * TODO: Silently failing for invalids. Do we to trigger an event or
                    * notify client in some way?
                    */
                    return;
                }
                // valid form & existing record. Update record
                else if (record) {
                    bform.updateRecord(record);
                }
                // form but no record, we need to add the record
                else if ((this.sourceStore.isFiltered() &&
                index >= this.sourceStore.totalLength) || !this.sourceStore.isFiltered()) {
                    var values = bform.getValues();
                    this.sourceStore.loadData([values], true);
                }
            }
        }
    },
    
    /**
     * Add a new tab
     */
    handleAddTab: function (cmp, newTab, oldTab) {
        if (this.addTab == newTab) {
            this.addFeedSource();
            // test if we have the max tabs
            if (this.items.length > this.maxFeedCount) {
                // hide the add tab
                this.hideTabStripItem(newTab);
            }
            return false;
        }
        return true;
    },
    
    /**
     * Add a feed source
     */
    addFeedSource: function (feedConfig) {
        var newRec, frm;
        if (feedConfig) {
            // add a record to the feed source store
            this.sourceStore.loadData([feedConfig], true);
            newRec = this.sourceStore.getAt(this.sourceStore.data.length - 1);
            frm = this.buildForm(feedConfig.title, newRec);
        } else {
            // build the blank form without a record
            frm = this.buildForm();
        }
        // insert it between last feed & add button
        this.insert(this.items.length - 1, frm);
        this.activate(frm);
    },
    
    /**
     * Build the add feed form
     */
    buildForm: function (title, record) {
        var index, stateid;
        var locked_feed = false;
        var default_secret = '';
        if (record) {
            index = record.store.indexOf(record) + 1;
            if (record.data && record.data.locked) {
                locked_feed = true;
            }
        } else {
            index = this.items.length;
            // setting the first feed's secret as a default for a new feed,
            // assuming that its valid and can access a hub
            var first_feed = Ext.StoreMgr.get('FeedSources').getAt(0);
            if (first_feed) {
                default_secret = first_feed.get('secret');
            }
        }
        if (this.items.length > 1 || this.items.itemAt(0) != this.addTab) {
            var lastIndex = (this.items.length > 1) ? (this.addTab) ?
                this.items.length - 2 : this.items.length - 1 : 0;
            var prevId = this.items.itemAt(lastIndex).stateId;
            stateid = 'F_' + String.fromCharCode(prevId.charCodeAt(2) + 1); 
        } else {
            stateid = 'F_A';
        }
        
        return new Ext.form.FormPanel({
            'title': '&#160;' + index + '&#160;&#160;&#160;',
            tabTip: title,
            // saves the state of each feed in a separate cookie, is setup in
            // initComponent
            stateful: this.stateful,
            stateId: stateid,
            closable: true,
            labelWidth: 75,
            layout: 'form',
            autoScroll: true,
            msgTarget: 'above',
            defaultType: 'textfield',
            defaults: {
                msgTarget: 'under'
            },
            bodyStyle: 'padding:5px',
            listeners: {
                'beforeclose': this.beforeRemoveTab,
                'afterrender': {
                    fn: function (cmp) {
                        var cb = cmp.accessComposite.innerCt.find('name', 'hideSecret')[0];
                        if (cb && cb.checked) {
                            this.switchSecretDisplay(cb, true);
                        }
                    },
                    delay: 300,
                    scope: this
                },
                scope: this
            },
            items: [{
                fieldLabel: 'Feed Name',
                name: 'title',
                ref: 'feedTitle',
                anchor: '97%',
                maxLength: 15,
                allowBlank: false,
                // don't allow changes if this feed record is locked
                readOnly: locked_feed,
                listeners: {
                    focus: function () {
                        if (locked_feed) {
                            Ext.Msg.alert('Locked Feed',
                                'This Default Feed cannot be modified.' +
                                ' It can only be temporarily disabled.');
                        }
                    }
                }
            }, {
                xtype: 'compositefield',
                // these references will be used within the tab itself
                ref: 'accessComposite',
                fieldLabel: 'Access Code',
                items: [{
                    fieldLabel: 'Access Code',
                    allowBlank: false,
                    name: 'secret',
                    minLength: 5,
                    xtype: 'textfield',
                    ref: '../secretField',
                    flex: 1,
                    invalidText: 'Field can not be blank and must be at least 5 characters long',
                    readOnly: locked_feed,
                    value: default_secret
                }, {
                    xtype: 'checkbox',
                    boxLabel: 'Hide',
                    name: 'hideSecret',
                    ref: '../hideSecret',
                    handler: this.switchSecretDisplay,
                    scope: this,
                    width: 50
                }]
            }, {
                fieldLabel: 'Feed URL',
                name: 'url',
                ref: 'feedUrl',
                allowBlank: false,
                anchor: '97%',
                readOnly: locked_feed
            }, {
                boxLabel: 'Disable Feed',
                name: 'disabled',
                xtype: 'checkbox',
                ref: 'disableCheck',
                handler: this.disableFeed,
                scope: this
            }, {
                xtype: 'displayfield',
                hideLabel: true,
                html: '<div style="padding: 5px;"></div>'
            }, {
                xtype: 'compositefield',
                hidden: locked_feed,
                items: [{
                    xtype: 'radio',
                    name: 'specialFeed',
                    boxLabel: 'Use Import Feed',
                    inputValue: 'import',
                    disabled: (VIEW.IMPORT_ENTRY_URL) ? false : true,
                    handler: function (cmp, checked) {
                        if (checked) {
                            cmp.ownerCt.ownerCt.importButton.enable();
                            var form_items = cmp.ownerCt.ownerCt.ownerCt;
                            form_items.feedTitle.setValue('Import');
                            form_items.feedUrl.setValue(VIEW.IMPORT_FEED_URL);
                        } else {
                            cmp.ownerCt.ownerCt.importButton.disable();
                        }
                    }
                }, {
                    xtype: 'button',
                    ref: 'importButton',
                    text: 'Import',
                    disabled: true,
                    handler: VIEW.show_entry_import
                }]
            }, {
                xtype: 'compositefield',
                hidden: locked_feed,
                items: [{
                    xtype: 'radio',
                    name: 'specialFeed',
                    boxLabel: 'Use Favorite Feed',
                    inputValue: 'favorite',
                    disabled: (VIEW.FAVORITE_ENTRY_URL) ? false : true,
                    handler: function (cmp, checked) {
                        if (checked) {
                            cmp.ownerCt.ownerCt.resetButton.enable();
                            var form_items = cmp.ownerCt.ownerCt.ownerCt;
                            form_items.feedTitle.setValue('Favorite');
                            form_items.feedUrl.setValue(VIEW.FAVORITE_FEED_URL);
                        }  else {
                            cmp.ownerCt.ownerCt.resetButton.disable();
                        }
                    }
                }, {
                    xtype: 'button',
                    ref: 'resetButton',
                    text: 'Reset',
                    disabled: true,
                    handler: function () {
                        Ext.Msg.confirm('Reset Favorites',
                            'Are you sure that you want to reset this feed?',
                            function (confirm) {
                                if (confirm === 'yes') {
                                    console.debug('Resetting favorites');
                                    Ext.Ajax.request({
                                        url: VIEW.FAVORITE_FEED_URL,
                                        params: { 'reset': 'yes' },
                                        success: function () {
                                            Ext.Msg.alert('Success', 'Favorites Reset');
                                        },
                                        failure: function (response) {
                                            console.error('Favorite reset error: ' +
                                                response.statusText);
                                            Ext.Msg.alert('Error',
                                                'Unable to reset Favorites');
                                        }
                                    });
                                }
                            }
                        );
                    }
                }]
            }]
        });
    },
    
    /**
     * Add by record
     */
    addByRecord: function (record) {
        var frm = this.buildForm(record.get('title'), record);
        if (this.addTab) {
            this.insert(this.items.length - 1, frm);
        } else {
            this.add(frm);
        }
        frm.getForm().loadRecord(record);
    },
    
    /**
     * Restore feed sources
     */
    restoreFeedSources: function (data) {
        if (data && data.length && data[0]) {
            this.un('beforetabchange', this.handleAddTab, this);
            this.items.each(function (item) {
                if (item != this.addTab) {
                    this.remove(item);
                }
            }, this);
            this.sourceStore.loadData(data);
            this.sourceStore.each(this.addByRecord, this);
            this.on('beforetabchange', this.handleAddTab, this);
        }
    },
    
    /**
     * Confirm before removing a feed
     */
    beforeRemoveTab: function (item) {
        Ext.Msg.confirm('Confirm', 'Do you really want to delete this feed source?',
            this.removeFeedSource, {panel: item.ownerCt, tab: item});
        // prevent the tab from being removed unless they confirm
        return false;
    },
    
    /**
     * Remove a feed tab
     */
    removeFeedSource: function (confirm) {
        if (confirm === 'yes') {
            var tabpanel = this.panel, item = this.tab;
            // get the url as the id
            var urlFld = item.form.findField('url');
            var url = (urlFld) ? urlFld.getValue() : null;
            if (url) {
                tabpanel.sourceStore.removeAt(tabpanel.sourceStore.indexOfId(url));
            }
            tabpanel.remove(item);
            // test if we have 1 less than the max tabs, if so, show the add tab
            if (tabpanel.items.length == tabpanel.maxFeedCount) {
                tabpanel.unhideTabStripItem(tabpanel.addTab);
            }
        }
    },
    
    /**
     * Switch secret between hidden and viewable
     */
    switchSecretDisplay: function (cmp, checked) {
        var codeFld = cmp.refOwner && cmp.refOwner.secretField;
        if (codeFld) {
            var pwd = codeFld.getValue();
            var codeConfig = Ext.applyIf({
                value: pwd,
                rendered: false,
                isDestroyed: false
            }, codeFld.initialConfig);
            var formPanel = (this.activeTab && this.activeTab.form) ? this.activeTab : null;
            if (formPanel) {
                var compositeFld = formPanel.accessComposite;
                if (checked) {
                    // destroy text box and re-render a password field
                    Ext.apply(codeConfig, {
                        xtype: 'field',
                        inputType: 'password'
                    });
                } else {
                    // destroy password field and re-render a text box
                    codeConfig.xtype = 'textfield';
                    codeConfig.inputType = undefined;
                }
                var compositeConfig = Ext.apply({}, compositeFld.initialConfig);
                compositeConfig.items[0] = codeConfig;
                compositeConfig.items[1].checked = checked;
                // back the ref off one level since one level gets added to it
                // during the insert procedure
                Ext.each(compositeConfig.items, function (item) {
                    item.ref = item.ref.split('/').slice(1).join('/');
                });
                formPanel.remove(compositeFld, true);
                // kludge to fix ticket #3578, the .remove() method removes the
                // composite field but not the actual form elements, this also
                // removes those elements - true fix is to fix .remove()
                formPanel.form.items.items.splice(3, 2);
                formPanel.insert(1, compositeConfig);
                formPanel.doLayout();
            }
        } else {
            cmp.on('afterrender', this, this.switchSecretDisplay.createDelegate(this,
                [cmp, checked]));
        }
    },
    
    /**
     * Temporarily disable a feed
     */
    disableFeed: function (cmp, checked) {
        var tab = new Ext.Element(cmp.refOwner.tabEl);
        var store = this.sourceStore;
        var url = cmp.refOwner.form.getFieldValues().url;
        if (!tab.dom && !cmp.refOwner.rendered) {
            cmp.refOwner.on('render', function () {
                tab = new Ext.Element(cmp.refOwner.tabEl);
                tab.toggleClass('disabledFeedTab');
                cmp.refOwner.form.el.toggleClass('disabledFeed');
            });
        } else {
            tab.toggleClass('disabledFeedTab');
            cmp.refOwner.form.el.toggleClass('disabledFeed');
        }
        var rec = store.getById(url);
        if (rec) {
            rec.set('disabled', checked);
        }
    },
    
    /**
     * Used to get the form values for saving state
     */
    getState: function () {
        var ids = [];
        this.items.each(function (item) {
            if (item != this.addTab) {
                ids.push(item.stateId);
            }
        }, this);
        // kill all previously saved tab contents
        var provider = Ext.state.Manager.getProvider();
        Ext.iterate(provider.state, function (k, v, states) {
            if (k.indexOf('F_') > -1) {
                provider.clear(k);
            }
        });
        return {tabs: ids, active: this.items.indexOf(this.getActiveTab())};
    },
    
    /**
     * Used to populate the form values for restoring state
     */
    applyState: function (state) {
        if (state.tabs.length) {
            var configs = [];
            var provider = Ext.state.Manager.getProvider();
            for (var i = 0, len = state.tabs.length; i < len; i++) {
                configs.push(provider.get(state.tabs[i]));
            }
            this.restoreFeedSources(configs);
        }
        this.activate(state.active);
    }
});

Ext.reg('mv_feedsettings', VIEW.Panel.FeedSettingsTabPanel);


/**
Display the entry import window, which allows the user to upload a ZIP file
containing an Entry and optional attachments.
*/
VIEW.show_entry_import = function () {
    var win_title = 'Import Entry';
    var win = new Ext.Window({
        title: win_title,
        closable: true,
        width: 500,
        height: 120,
        layout: 'fit',
        items: []
    });
    var filePanel = new Ext.FormPanel({
        fileUpload: true,
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'fileuploadfield',
            fieldLabel: '<b>File</b>',
            name: 'attachment-file',
            id: 'attachment-file',
            emptyText: 'Select a ZIP file to import',
            width: 350
        }],
        buttons: [{
            text: 'Import',
            handler: function (cmp) {
                if (filePanel.getForm().isValid()) {
                    filePanel.getForm().submit({
                        url: VIEW.IMPORT_ENTRY_URL,
                        submitEmptyText: false,
                        waitMsg: 'Uploading the file...',
                        success: function (form, response) {
                            console.debug('Imported entry');
                            Ext.Msg.alert('Success', 'Import Entry Complete');
                            // access window this way as form doesn't have
                            // ownerCt, ref, or other common methods
                            win.close();
                        },
                        failure: function (form, response) {
                            console.error('Import Error: ' +
                                response.result.message);
                            Ext.Msg.alert('Import Error',
                                'Unable to upload the file.  ' +
                                    response.result.message);
                            win.close();
                        }
                    });
                }
            }
        }]
    });
    // adding to items this way to allow access to win by form handlers
    win.add(filePanel);
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};
