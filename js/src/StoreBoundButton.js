/**
MASAS View Tool - StoreBoundButton
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines an Ext helper class to create a split button, where
it includes a drop down menu tied to loading individual stores.  Used by
LoadFeedButton.js
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

/**
 * Store bound button
 */
Ext.StoreBoundButton = Ext.extend(Ext.SplitButton, {
    /**
     * Config
     */
    store: null,
    itemType: 'menuitem',
    itemBinding: null,
    itemHandler: null,
    
    /**
     * Initialize
     */
    initComponent: function () {
        if (this.store && !(this.store) instanceof Ext.data.Store) {
            this.store = new Ext.data.Store(this.store);
        }
        
        if (!this.itemBinding) {
            this.itemBinding = {text: 'text'};
        } else if (Ext.isString(this.itemBinding)) {
            this.itemBinding = {text: this.itemBinding};
        }
        
        if (!this.menu) {
            this.menu = new Ext.menu.Menu();
        } else if (!(this.menu) instanceof Ext.menu.Menu) {
            this.menu = new Ext.menu.Menu(this.menu);
        }
        
        Ext.StoreBoundButton.superclass.initComponent.call(this);
        
        if (this.store) {
            this.bindStore();
        }
    },
    
    /**
     * Destroy button
     */
    destroy: function () {
        this.unbindStore();
        Ext.StoreBoundButton.superclass.destroy.apply(this, arguments.slice(0));
    },
    
    /**
     * Bind events to the store
     */
    bindStore: function () {
        this.store.on({
            'add': this.onStoreItemAdd,
            'remove': this.onStoreItemRemove,
            'update': this.onStoreItemUpdate,
            'metaChange': this.onStoreMetachange,
            scope: this
        });
        this.store.each(this.addMenuItem, this);
    },
    
    /**
     * Unbind events from the store
     */
    unbindStore: function () {
        this.store.un({
            'add': this.onStoreItemAdd,
            'remove': this.onStoreItemRemove,
            'update': this.onStoreItemUpdate,
            'metaChange': this.onStoreMetachange,
            scope: this
        });
    },
    
    /**
     * Add a menu item
     */
    addMenuItem: function (record) {
        var propKeys = Ext.getKeys(this.itemBinding);
        var config = {};
        for (var i = 0, len = propKeys.length; i < len; i++) {
            var k = propKeys[i];
            config[k] = record.get(this.itemBinding[k]);
        }
        Ext.applyIf(config, {
            xtype: this.itemType,
            handler: this.itemHandler,
            recordId: record.id
        });
        return this.menu.addMenuItem(config);
    },
    
    /**
     * Remove a menu item
     */
    removeMenuItem: function (record) {
        var item = this.menu.find('recordId', record.id);
        if (item[0]) {
            this.menu.remove(item[0]);
        }
        return item[0];
    },
    
    /**
     * Add a store item
     */
    onStoreItemAdd: function (store, records, ndx) {
        for (var i = 0, len = records.length, items = []; i < len; i++) {
            items[i] = this.addMenuItem(records[i]);
        }
        return items;
    },
    
    /**
     * Remove a store item
     */
    onStoreItemRemove: function (store, record, ndx) {
        return this.removeMenuItem(record);
    },
    
    /**
     * Store item update
     */
    onStoreItemUpdate: function (store, record, op) {
        var item = this.menu.find('recordId', record.id);
        if (item[0]) {
            item[0].setText(record.get(this.itemBinding.text));
        }
        return item[0];
    },
    
    /**
     * Store meta change
     */
    onStoreMetachange: function (store, meta) {
        this.unbindStore();
        this.menu.removeAll(true);
        this.bindStore();
    }
});

Ext.reg('boundsplitbutton', Ext.StoreBoundButton);
