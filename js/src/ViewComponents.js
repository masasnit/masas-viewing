/**
MASAS View Tool - ViewComponents
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines a grab-bag of application handlers in the global 'VIEW'
namespace.  This includes feature select and hover handlers, which 
delegate to cluster or individual handlers depending on if the features
are in a cluster or not.  Some methods are defined for finding features and
removing features from a feed and cleaning up a map.  Methods are provided
to zooming the map to highlight features or clusters or saved views
controller methods defined to control loading of all or single feed.
Methods defiend to render feed content to popup windows.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * App variable initial config.  ViewComponents.js is loaded first and so sets
 * up some variables needed by other modules.
 */
Ext.apply(VIEW, {
    map: null,
    mapPanel: null,
    header: null,
    settingsPanel: null,
    gridPanel: null,
    contentWindow: null,
    // to prevent zooming in too far to a single point, offset the zoom back
    // out this amount
    pointZoomOffset: 6,
    // max height for popups, will add scrollbars if more info
    popupMaxHeight: 200,
    // adjust the location for an icon, advanced settings, default is true
    adjustIcons: true,
    // automatic reloading variables
    autoLoadCount: 0,
    autoLoadTask: null
});


/**
 * This is the main select feature handler, delegates to the appropriate handler
 */
VIEW.feature_selected = function (evt) {
    // normal screen select feature event
    if (!evt.targetFeature) {
        if (evt.feature.cluster) {
            if (evt.feature.cluster.length > 1) {
                VIEW.show_cluster_popup(evt.feature);
            } else {
                VIEW.show_feature_popup(evt.feature.cluster[0]);
            }
        } else {
            VIEW.show_feature_popup(evt.feature);
        }
    }
    // selected from the grid
    else {
        var feature = null;
        if (evt.targetFeature.cluster) {
            feature = evt.feature;
            VIEW.show_feature_popup(feature);
        } else {
            feature = evt.targetFeature;
            // selecting a single feature from inside a cluster
            VIEW.show_feature_popup(feature, evt.feature.layer, evt.feature);
        }
    }
};


/**
 * Feature unselect handler
 */
VIEW.feature_unselected = function (evt) {
    if (evt.targetFeature) {
        // needed for when switching between entries using the grid
        // to show popups
        //TODO: why is the generic cleanup method needed and the
        //      normal close_feature_popup not working?
        VIEW.clean_up_map();
    } else {
        // used with the popup hover in/out and close
        VIEW.close_feature_popup(evt);
    }
};


/**
 * Opens a popup for clustered features
 */
VIEW.show_cluster_popup = function (feature) {
    var map = feature.layer.map || VIEW.map;
    var popup = new GeoExt.Popup({
        html: VIEW.Template.Cluster_Tip_Template.apply(feature.cluster),
        title: VIEW.Template.Cluster_Title_Template.apply(feature),
        unpinnable: false,
        location: feature,
        closable: true,
        anchored: true,
        map: map,
        ref: 'popup',
        id: 'popup_' + feature.id,
        width: 350,
        minHeight: 100,
        autoScroll: true,
        panIn: false,
        listeners: {
            'close': function (cmp) {
                VIEW.close_feature_popup({'feature': feature});
            }
        }
    });
    VIEW.mapPanel.add(popup);
    VIEW.mapPanel.popup = popup;
    popup.show();
    if (popup.getHeight() > VIEW.popupMaxHeight) {
        popup.setHeight(VIEW.popupMaxHeight);
        popup.panIntoView();
    }
};


/**
 * Opens a popup to show feature summary
 */
VIEW.show_feature_popup = function (feature, layer, cluster) {
    var map = (layer && layer.map) || feature.layer.map || VIEW.map;
    var mapExtent = map.getExtent();
    var popup = null;
    var focusFeat = null;
    var detailed_popup = VIEW.settingsPanel.advancedSettings.detailedPopups.getValue();
    // skipping popup for entry with already open content window
    if (VIEW.contentWindow) {
        if (feature.cluster) {
            // assuming single feature clusters are the norm for standalone
            // icons on the map, otherwise is an actual cluster
            if (feature.cluster.length === 1) {
                if (VIEW.contentWindow.panels.Atom.entryID === feature.cluster[0].data.id) {
                    return;
                }
            }
        } else {
            if (VIEW.contentWindow.panels.Atom.entryID === feature.data.id) {
                return;
            }
        }
    }
    if (feature && cluster) {
        // I think this is creating a new feature/icon that will appear
        // on top of the cluster to distinguish it and using that
        // new feature as the target for the popup
        var cloned = feature.clone();
        cloned.fid = feature.fid;
        cloned.cloned = true;
        cluster.layer.addFeatures([cloned], {silent: true});
        focusFeat = cloned;
    } else {
        focusFeat = feature.clone();
    }
    // attributes to use for generating the content templates
    var attrs = feature.attributes;
    // adding the original updated datetime in UTC as the normal updated value
    // has been converted to local
    attrs['original_updated'] = feature.data.updated;
    // apply popup content templates
    var content = (detailed_popup) ?
        VIEW.Template.Detailed_Popup_Template.apply(attrs) :
        VIEW.Template.Simple_Popup_Template.apply(attrs);
    var title = VIEW.Template.Popup_Title_Template.apply(attrs);
    if (feature.originalGeom) {
        var geom = feature.originalGeom.clone();
        var tempFeature = new OpenLayers.Feature.Vector(geom, Ext.apply({}, attrs));
        VIEW.mapLayers.MASASLayerHighlight.addFeatures([tempFeature]);
        /* NOTE: in SVN Revision 184:185 madir committed a change to stop the
        check below.  His comment was "re #3388: don't re-assign the feature
        location for the popup, features are already in adjusted locations."
        
        JW modified this check to move the icon, to which the popup will be
        attached, only when adjustIcons is off, accomodating large geoms
        where a part may be in view and able to be shown.
        */
        if (!VIEW.adjustIcons) {
            // test if original geom is contained in the viewport
            var geomBounds = geom.getBounds();
            if (!mapExtent.containsBounds(geomBounds) &&
            !geomBounds.containsBounds(mapExtent)) {
                var nearest = VIEW.get_nearest_vertex(mapExtent.toGeometry().getCentroid(),
                    geom).clone();
                if (mapExtent.containsLonLat(new OpenLayers.LonLat(nearest.x,
                nearest.y))) {
                    focusFeat.geometry = nearest;
                }
                if (focusFeat.layer) {
                    focusFeat.layer.drawFeature(focusFeat);
                }
            }
        }
    }
    // if the intended popup location isn't able to be viewed, don't
    // show it all.  This resolves problems where the map would try
    // a significant pan to the feature location and the user would end up
    // with a blank screen.  Assumes .geometry is always a point.
    //NOTE: if this kind of check is needed after generating a GeoExt popup
    //      the insideViewport attribute can be used.
    if (!mapExtent.contains(focusFeat.geometry.x, focusFeat.geometry.y)) {
        //TODO: no cleanup right now of any features added above as they
        //      should be cleaned up with next popup load.  Make sure
        //      this is okay with further testing.
        return;
    }
    
    popup = new GeoExt.Popup({
        html: content,
        title: title,
        unpinnable: false,
        location: focusFeat,
        closable: true,
        anchored: true,
        map: map,
        ref: 'popup',
        id: 'popup_' + feature.id,
        width: (detailed_popup) ? 500 : 350,
        minHeight: 100,
        autoScroll: true,
        panIn: false,
        listeners: {
            'close': function (cmp) {
                VIEW.close_feature_popup({'feature': focusFeat});
            }
        }
    });
    
    VIEW.mapPanel.add(popup);
    VIEW.mapPanel.popup = popup;
    popup.show();
    if (popup.getHeight() > VIEW.popupMaxHeight) {
        popup.setHeight(VIEW.popupMaxHeight);
        popup.panIntoView();
    }
    
    // highlight the grid record
    var view = VIEW.gridPanel.getView();
    var rowId = VIEW.gridPanel.getStore().findExact('feature', feature);
    if (rowId > -1) {
        view.focusRow(rowId);
        Ext.fly(view.getRow(rowId)).addClass('x-grid3-row-selected');
    }
};


/**
 * Close a popup
 */
VIEW.close_feature_popup = function (evt) {
    var feature, popup, featureId;
    feature = evt.feature;
    // skipping popup for entry with already open content window
    if (VIEW.contentWindow) {
        if (feature.cluster) {
            // assuming single feature clusters are the norm for standalone
            // icons on the map, otherwise is an actual cluster
            if (feature.cluster.length === 1) {
                if (VIEW.contentWindow.panels.Atom.entryID === feature.cluster[0].data.id) {
                    return;
                }
            }
        } else {
            if (VIEW.contentWindow.panels.Atom.entryID === feature.data.id) {
                return;
            }
        }
    }
    popup = VIEW.mapPanel.popup || Ext.getCmp('popup_' + feature.id);
    if (popup) {
        VIEW.mapPanel.popup = null;
        popup.destroy();
        if (feature) {
            feature.popup = null;
            //TODO: not sure what feature.highlight does?
            feature.highlight = false;
            if (!feature.cluster && feature.cloned && feature.layer) {
                feature.layer.removeFeatures([feature]);
            }
        }
    }
    // ensure grid is unfocused
    /* NOTE: in SVN Revision 186:187 madair commited a change to prevent error
    messages about feature.cluster not existing when using the close box on
    a popup window.  His comment was "closes #409: add check for
    feature.cluster so the cose popup doesn't fail".
    
    JW added additional brackets around this cluster check as that was also
    failing.
    */
    featureId = feature.fid || ((feature.cluster) ? feature.cluster[0].fid : null);
    if (featureId) {
        var row = VIEW.gridPanel.getStore().find('fid', featureId);
        if (row > -1) {
            Ext.fly(VIEW.gridPanel.getView().getRow(row)).removeClass(
                'x-grid3-row-selected');
        }
    }
    VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
};


/**
 * Searches for the first feature that has the specified attribute value
 * (defaults to the 'id' attribute)
 */
VIEW.find_feature_in_clusters = function (id, attr) {
    if (!attr) {
        attr = 'id';
    }
    var features = VIEW.mapLayers.MASASLayer.features;
    var foundFeature = null;
    for (var i = 0; i < features.length; ++i) {
        if (features[i][attr] === id) {
            foundFeature = features[i];
            break;
        }
        for (var j = 0; j < features[i].cluster.length; ++j) {
            var feature = features[i].cluster[j];
            if (feature[attr] === id) {
                foundFeature = feature;
                break;
            }
        }
        if (foundFeature) {
            break;
        }
    }
    return foundFeature;
};


/**
 * Locates the vertex closest to the reference point (both are geometries)
 */
VIEW.get_nearest_vertex = function (refPt, geometry) {
    var vertices = geometry.getVertices();
    var dist = Number.MAX_VALUE;
    var closest = null;
    var center = refPt.getCentroid();
    for (var v = 0; v < vertices.length; ++v) {
        var testDist = vertices[v].distanceTo(center);
        if (testDist < dist) {
            dist = testDist;
            closest = v;
        }
    }
    return vertices[closest];
};


/**
 * Cleanup the map.  Attached to the map's zoomend listener.
 */
VIEW.clean_up_map = function (evt) {
    // remove popups
    if (VIEW.mapPanel.popup) {
        VIEW.mapPanel.popup.destroy();
    }
    // deselect features as needed
    if (VIEW.mapLayers.MASASLayer.selectedFeatures.length) {
        var layer = VIEW.mapLayers.MASASLayer;
        while (layer.selectedFeatures.length) {
            var feat = layer.selectedFeatures.pop();
            feat.renderIntent = 'default';
            layer.events.triggerEvent("featureunselected", {feature: feat});
            layer.drawFeature(feat);
        }
    }
    // remove features not processed through cluster strategy
    // i.e. features we add progamatically from grid selection event
    var unclustered = VIEW.mapLayers.MASASLayer.getFeaturesByAttribute('count',
        undefined);
    if (unclustered.length) {
        // OK to use destroy here because we used cloned feature(s) and
        // didn't add it to the grid
        VIEW.mapLayers.MASASLayer.destroyFeatures(unclustered);
        // clustering can stop working properly after this destroy, so this
        // refilter should correct any clusters that aren't working
        VIEW.gridPanel.featureStore.filterFeatures(VIEW.gridPanel.featureStore);
    }
    if (VIEW.mapLayers.MASASLayerHighlight.features.length) {
        VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
    }
};


/**
 * Zoom to a feature
 */
VIEW.zoom_to_feature = function (fid, source) {
    var foundFeature = VIEW.find_feature_in_clusters(fid, 'fid'); 
    if (foundFeature) {
        var geometry = foundFeature.originalGeom;
        /* NOTE: in SVN Revision 187:188 madair commited this change.
        His comment was "closes #3644: the zoomTo features causes reclustering
        which triggers featuresAdded event which causes the grid to be re-loaded.
        This change prevents that action". */
        // kludge to prevent the featuresAdded event from reloading the grid after
        // a zoomTo call which re-clusters the features, resulting in the
        // grid scrolling back to the top instead of where it was clicked
        VIEW.gridPanel.featureStore._adding = true;
        if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
            VIEW.map.zoomToExtent(geometry.getBounds());
        } else {
            var zoom = VIEW.map.baseLayer.maxZoomLevel - this.pointZoomOffset;
            VIEW.map.setCenter(new OpenLayers.LonLat(geometry.x, geometry.y), zoom);
        }
        delete VIEW.gridPanel.featureStore._adding;
    }
    // the popup hasn't been created yet after this zoom so this doesn't
    // close the expected new popup, but does close any older ones.  So
    // that may be why its being called?  Unknown if actually needed.
    VIEW.close_feature_popup({feature: foundFeature});
};


/**
 * Zoom to a cluster
 */
VIEW.zoom_to_cluster = function (id, layerId) {
    var mainFeature = VIEW.mapLayers.MASASLayer.getFeatureById(id);
    var geometry = new OpenLayers.Geometry.Collection();
    for (var j = 0; j < mainFeature.cluster.length; ++j) {
        var feature = mainFeature.cluster[j];
        geometry.addComponents(feature.originalGeom.clone());
    }
    geometry.calculateBounds();
    VIEW.map.zoomToExtent(geometry.getBounds());
    VIEW.close_feature_popup({feature: mainFeature});
};


/**
 * Select and show an individual feature out of a cluster listing
 */
VIEW.highlight_cluster_feature = function (id) {
    var store = Ext.StoreMgr.get('FeedStore');
    var entryNdx = store.find('id', id);
    if (entryNdx > -1) {
        var rec = store.getAt(entryNdx);
        if (rec) {
            var feat = rec.getFeature();
            //VIEW.mapControls.highlightControl.highlight(rec.getFeature());
            feat.renderIntent = 'select';
            //tempFeature.originalGeom = record.data.feature.originalGeom;
            var cloned = feat.clone();
            cloned.cloned = true;
            VIEW.mapLayers.MASASLayer.addFeatures([cloned], {silent: true});
            VIEW.mapLayers.MASASLayerHighlight.addFeatures(
                new OpenLayers.Feature.Vector(feat.originalGeom.clone()));
            VIEW.mapPanel.popup.destroy();
            VIEW.mapPanel.popup = null;
            VIEW.show_content_window(id, rec.get('source_url'));
        }
    }
};


/**
 * Loads all enabled feeds
 *
 * @param {Object} - FeedSources Store
 */
VIEW.load_all_feeds = function (store) {
    store.loadCounter = 0;
    store.feedLoadError = null;
    var feed_list = [];
    store.each(function (record) {
        var rec_disabled = record.get('disabled');
        var rec_secret = record.get('secret');
        if (!rec_disabled && rec_secret) {
            store.loadCounter += 1;
            feed_list.push(record);
        } else {
            if (!rec_secret) {
                alert('You must enter an Access Code for the ' +
                    record.get('title') + ' Feed');
            }
        }
    });
    if (store.loadCounter > 0) {
        Ext.Msg.show({
            title: 'Load Feed',
            msg: 'Loading ' + store.loadCounter + ' Feeds',
            modal: false,
            progress: true,
            wait: true,
            width: 200
        });
        // reset/clear so that all Entries from all Feeds are newly 'add'ed
        VIEW.cluster.clearCache();
        VIEW.gridPanel.store.loadData([], false);
        // loads all feeds at the same time, async completion
        for (var i = 0; i < feed_list.length; i++) {
            VIEW.load_masas_feed(feed_list[i]);
        }
    } else {
        Ext.Msg.alert('Feed Error', 'All Feeds are disabled. Please enable' +
            ' at least one Feed.');
    }
};


/**
 * Load a single feed, such as from the button drop-down
 *
 * @param {Object} - FeedSources Store
 * @param {Integer} - Feed ID
 */
VIEW.load_single_feed = function (store, feed_id) {
    var record = store.getById(feed_id);
    if (record) {
        var rec_disabled = record.get('disabled');
        var rec_secret = record.get('secret');
        if (!rec_disabled && rec_secret) {
            store.loadCounter = 1;
            store.feedLoadError = null;
            Ext.Msg.show({
                title: 'Load Feed',
                msg: 'Loading ' + record.get('title') + ' Feed',
                modal: false,
                progress: true,
                wait: true,
                width: 200
            });
            // unlike load_all_feeds which resets the whole store, this does
            // not, so that other feeds' Entries aren't removed, with new
            // Entries from this feed added and duplicates for existing ones
            // removed
            VIEW.load_masas_feed(record);
        } else {
            if (!rec_secret) {
                Ext.Msg.alert('Feed Error', 'You must enter an Access Code');
            }
        }
    } else {
        Ext.Msg.alert('Feed Error', 'Cannot load Feed');
    }
};


/**
 * Load a feed, building the URL and parameters required
 *
 * @param {Object} - feed Record
 */
VIEW.load_masas_feed = function (record) {
    var map = VIEW.map;
    var url = record.get('url');
    var title = record.get('title');
    var params = {};
    var settings = VIEW.settingsPanel.userSettings;
    var timeFilter = VIEW.settingsPanel.timeSettings;
    var authorSettings = VIEW.settingsPanel.authorSettings;
    var hide_icons = VIEW.settingsPanel.advancedSettings.hideIcons.getValue();
    var s_val = record.get('secret');
    params['secret'] = s_val;
    
    console.debug('Load Feed: ' + url);
    
    /*TODO: this is likely no longer needed, see notes in clear_feed_features
    VIEW.cluster.deactivate();
    VIEW.clear_feed_features(url);
    VIEW.cluster.activate();
    */
    
    // by default only the exact map viewport is loaded.  An option is to add
    // a buffer around that extent using   .scale(1.3)  which will then load
    // some additional surrounding Entries, but not currently enabled.
    var current_view_bounds = map.getExtent();
    // update the load feed extent on the map
    VIEW.mapLayers.loadFeed.removeAllFeatures();
    var load_geometry = current_view_bounds.toGeometry();
    var load_feature = new OpenLayers.Feature.Vector(load_geometry);
    VIEW.mapLayers.loadFeed.addFeatures([load_feature]);
    // add geospatial bbox query to Hub request
    current_view_bounds.transform(map.getProjectionObject(),
        new OpenLayers.Projection('EPSG:4326'));
    params['bbox'] = current_view_bounds.toBBOX(4);
    
    var l_val = settings.getForm().getValues().languageSelect;
    params['lang'] = l_val;
    
    // convert browser's local time to UTC
    function to_utc_time(dtime) {
        var dtime_adj = Date.parseDate(dtime, 'Y-m-d\\TH:i');
        var offset = new Date().getTimezoneOffset() / -60;
        // reverse the offset in order to go back to UTC
        // ie.  -5 for EDT should be +5 to get the UTC
        var offset_utc = offset - (offset * 2);
        dtime_adj = VIEW.adjust_time(dtime_adj, offset_utc);
        return Ext.util.Format.date(dtime_adj, 'Y-m-d\\TH:i:s\\Z');
    }
    
    var timeFilterValues = timeFilter.getForm().getValues();
    switch (timeFilterValues.timeSelect) {
    case 'since':
        var sd_val = timeFilterValues.sinceDT;
        var st_val = timeFilterValues.sinceTM;
        if (sd_val && st_val) {
            params['dtsince'] = to_utc_time(sd_val + 'T' + st_val);
        }
        break;
    case 'range':
        var rsd_val = timeFilterValues.startDT;
        var rst_val = timeFilterValues.startTM;
        var red_val = timeFilterValues.endDT;
        var ret_val = timeFilterValues.endTM;
        if (rsd_val && rst_val && red_val && ret_val) {
            params['dtstart'] = to_utc_time(rsd_val + 'T' + rst_val);
            params['dtend'] = to_utc_time(red_val + 'T' + ret_val);
        }
        break;
    }
    // the datetime search value is defaulted to updated by the Hub, so add
    // if its different
    if (timeFilterValues.searchType && timeFilterValues.searchType !== 'updated' &&
    timeFilterValues.timeSelect !== 'current') {
        params['dtval'] = timeFilterValues.searchType;
    }
    
    if (authorSettings.authorFilter.filterSetting) {
        var author_list = [];
        authorSettings.authorList.store.each( function (rec) {
            author_list.push(rec.data.uri);
        });
        if (author_list.length > 0) {
            if (authorSettings.authorFilter.filterSetting === 'hide') {
                params['not_author'] = author_list.join();
            } else if (authorSettings.authorFilter.filterSetting === 'show') {
                params['author'] = author_list.join();
            }
        }
    }
    
    params['limit'] = Ext.getCmp('entryDisplayLimit').getValue();
    
    // finally, merge with any baseParams on the store
    Ext.applyIf(params, VIEW.gridPanel.store.baseParams);
    
    // use the proxy if available and this is an absolute URL versus
    // a local relative URL
    if (VIEW.AJAX_PROXY_URL && Ext.form.VTypes.url(url)) {
        params = {
            'url': Ext.urlAppend(url, Ext.urlEncode(params)),
            'timeout': 30
        };
        url = null;
    }
    
    // cache buster will be filtered out by the proxy and not passed to hub
    params['_dc'] = new Date().getTime();
    
    // turn on layer if needed
    //TODO: if problems arise with users forgetting to un-hide the
    //      icons, may need to have a warning popup to remind them
    if (!VIEW.mapLayers.MASASLayer.visibility && !hide_icons) {
        VIEW.mapLayers.MASASLayer.setVisibility(true);
    }
    
    // update the feed store
    VIEW.gridPanel.store.load({
        'params': params,
        sourceId: record.id,
        sourceTitle: title,
        failure: VIEW.handle_feed_errors,
        callback: VIEW.handle_loaded_feed,
        scope: record.store,
        add: true,
        'url': url || VIEW.AJAX_PROXY_URL
    });
};


/**
 * Load feed complete handler, regardless of loading success.  Note that
 * the scope for 'this' is set to the FeedSources store
 *
 * @param {Array} - newly added Feed Records
 * @param {Object} - options provided to .store.load()
 * @param {Boolean} - load success
 */
VIEW.handle_loaded_feed = function (records, options, success) {
    // each feed has completed loading, keeping track as they async finish
    this.loadCounter -= 1;
    if (!success) {
        // save the feed(s) with errors for display when all loads are complete
        if (this.feedLoadError) {
            this.feedLoadError = this.feedLoadError + ', ' + options.sourceTitle;
        } else {
            this.feedLoadError = options.sourceTitle;
        }
    }
    if (this.loadCounter === 0) {
        // all feeds are now finished their async loads and any final operations
        // can now be run
        Ext.Msg.hide();
        if (this.feedLoadError) {
            Ext.Msg.alert('Feed Error', 'Error loading ' + this.feedLoadError);
        } else {
            var featureStore = Ext.StoreMgr.get('FeedStore');
            var max_results = parseInt(Ext.getCmp('entryDisplayLimit').getValue(),
                10);
            if (!featureStore.totalLength) {
                Ext.Msg.alert('No Entries', 'No Entries were found for this view');
            } else if (featureStore.totalLength >= max_results) {
                // this max results warning will appear for the sum total of
                // entries for all Feeds as it not only warns about possible
                // truncation for an individual Feed, but browser performance
                // issues as well.
                Ext.Msg.alert('Maximum Entries Exceeded',
                    '<b>The maximum number of map Entries has been reached for' +
                    ' this view.<br>' + 'Only the ' + max_results +
                    ' most recent Entries may be displayed.</b><br><br>Please' +
                    ' use the map to zoom in to an area of interest to reduce' +
                    ' the number of Entries needing to be displayed on the map.' +
                    ' <br><br>If necessary, you can adjust the Entry Display' +
                    ' Limit in the Advanced settings panel.');
            }
        }
    }
};


/**
 * Additional handling for feed errors in order to capture and log the actual
 * error response.  Called prior to handle_loaded_feed.
 *
 * @param {Object} - XHR response
 */
VIEW.handle_feed_errors = function (resp) {
    var status_num = parseInt(resp.status, 10);
    // handle_loaded_feed should already notify the user, special errors
    // may need additional notification
    if (status_num === 401) {
        // since Ext.Msg is a singleton and it may need to stay in place while
        // another feed loads, using window.alert instead
        alert('Feed Error - Access Code is not valid or authorized');
    }
    //TODO: special notifications for 408 timeouts as well?
    console.error('Feed Error - ' + resp.status + ': ' + resp.responseText);
};


/**
 * Config for automatic reloading Task that is managed using Ext.TaskMgr
 */
VIEW.autoLoadConfig = {
    run: function () {
        var load_interval = VIEW.settingsPanel.userSettings.autoLoadInterval.getValue();
        // 0 indicates disabled and this task shouldn't even be running or there
        // may be a problem getting the interval, so in both cases don't run
        if (!load_interval) {
            return;
        }
        load_interval = parseInt(load_interval, 10);
        // increment the counter, this task runs every minute and so once it
        // reaches the desired load interval, it will load the feeds
        VIEW.autoLoadCount += 1;
        if (VIEW.autoLoadCount === load_interval) {
            console.debug('Automatic Reload');
            VIEW.load_all_feeds(Ext.StoreMgr.get('FeedSources'));
            // reset for next reload
            Ext.getDom('autoReloadTime').innerHTML = '&nbsp;&nbsp;|&nbsp;&nbsp;' +
                'Auto Load in ' + load_interval + ' minutes';
            VIEW.autoLoadCount = 0;
        } else {
            // update the countdown to the next reload
            var time_remaining = load_interval - VIEW.autoLoadCount;
            Ext.getDom('autoReloadTime').innerHTML = '&nbsp;&nbsp;|&nbsp;&nbsp;' +
                'Auto Load in ' + time_remaining + ' minutes';
        }
    },
    // runs every minute
    interval: 60000
};


/**
 * Open a content window
 * 
 * @param {String} - the Atom <id> for an Entry
 * @param {String} - the ID for a source feed
 * @param {String} - the tab to open upon creation, ie Atom, CAP, Attachments
 */
VIEW.show_content_window = function (id, sourceId, tab) {
    var store = Ext.StoreMgr.get('FeedStore');
    var entryNdx = store.find('id', id);
    if (entryNdx > -1) {
        VIEW.show_entry_content(store.getAt(entryNdx).get('links'), sourceId, tab);
    }
};


/**
 * Populate a content window
 *
 * @param {String} - the links object for an Entry, used to load the data for
 *                   populating the content tabs
 * @param {String} - the ID for the source feed, used to get the Hub url and secret
 * @param {String} - the tab to open upon creation, ie Atom, CAP, Attachments
 */
VIEW.show_entry_content = function (links, sourceId, tab) {
    var feedRec = Ext.StoreMgr.get('FeedSources').getById(sourceId);
    if (VIEW.contentWindow && VIEW.contentWindow.links.Atom.href !== links.Atom.href) {
        // existing content window will be replaced with new info, clean up first
        VIEW.contentWindow.close();
    }
    if (!VIEW.contentWindow) {
        //var mapBox = VIEW.mapPanel.getEl().getBox();
        VIEW.contentWindow = new VIEW.ContentWindow({
            closeAction: 'close',
            'links': links,
            feedInfo: feedRec,
            listeners: {
                destroy: function () {
                    // a handler attached to the close listener normally deals
                    // with cleanup for these windows, I think this was added
                    // for cases where close doesn't complete properly?
                    VIEW.contentWindow = null;
                }
            }
        });
    } else {
        // default back to Atom message if we already have an open window
        // and no special tab indicated
        tab = tab || 'messages|Atom';
    }
    // show window
    VIEW.contentWindow.show();
    // open to a specified tab and any tab specific instructions
    if (tab) {
        var parts = tab.split('|');
        var group = parts[0]; 
        tab = parts[1];
        var panel = VIEW.contentWindow.panels[tab] || VIEW.contentWindow.panels['Image'] ||
            VIEW.contentWindow.panels['External'];
        // handle dot delimited tab panels (Source.Atom,Image.1,External.0,etc...)
        if (tab && tab.indexOf('.') > -1) {
            parts = tab.split('.');
            panel = VIEW.contentWindow.panels[parts[0]];
            for (var i = 0; i < parts.length; i++) {
                panel = panel[parts[i]];
            }
        } else if (Ext.isArray(panel)) {
            panel = panel[0];
        }
        VIEW.contentWindow.tabs.setActiveGroup(VIEW.contentWindow.tabs[group]);
        VIEW.contentWindow.tabs.activeGroup.setActiveTab(panel);
    }
};


/**
 * After the content window is created and the Atom content is loaded, show the
 * Entry's Atom geometry on the map.
 *
 * NOTE: if the content fails to load properly, bad download/template then
 * this method won't be run and the popup will remain in place to show what
 * was attempted to be loaded
 *
 * @param {String} - the Atom <id> for an Entry
 */
VIEW.show_entry_geometry = function (entryID) {
    VIEW.mapLayers.MASASLayerEntry.destroyFeatures();
    var store = Ext.StoreMgr.get('FeedStore');
    var grid = VIEW.gridPanel.getView();
    var rec_idx = store.find('id', entryID);
    if (rec_idx > -1) {
        var entry_rec = store.getAt(rec_idx);
        // add the entry's geometry to the map for display
        if (entry_rec.data.feature.originalGeom) {
            var orig_geom = entry_rec.data.feature.originalGeom.clone();
            var geom_feature = new OpenLayers.Feature.Vector(orig_geom);
            // normally the style for geometry excludes points
            // because of hover/popup not wanting to see them, but
            // in this case we do
            if (orig_geom.CLASS_NAME === 'OpenLayers.Geometry.Point') {
                geom_feature.style = OpenLayers.Util.applyDefaults(
                    {pointRadius: 6},
                    VIEW.layerStyles.geometry_style.defaultStyle);
            }
            VIEW.mapLayers.MASASLayerEntry.addFeatures([geom_feature]);
        }
        //TODO: log if originalGeom missing?
        // highlight the row in the grid, the store index and the
        // grid index should be the same
        grid.focusRow(rec_idx);
        Ext.fly(grid.getRow(rec_idx)).addClass('x-grid3-row-selected');
        //TODO: find a way to properly highlight the icon too.  Several
        //      methods were tried without success.  Adding the icon
        //      to the LayerEntry doubled the icons up and didn't look
        //      good.  Changing the render intent of this store's feature's
        //      icon also caused doubling since it was in the cluster and
        //      not the original feature the icon was attached to.
        
        // using a little delay before closing the popup to make it clearer
        // what feature this content window applies too
        setTimeout(function () {
            if (VIEW.mapPanel.popup) {
                VIEW.mapPanel.popup.destroy();
            }
            if (VIEW.mapLayers.MASASLayerHighlight.features.length) {
                VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
            }
        }, 1000);
    }
};


/**
 * Show or Hide any polygon(s) or circle(s) associated with a CAP area block
 * 
 * @param {String} - the areaid for a particular area block
 */
VIEW.show_CAP_area = function (areaid) {
    var elem = Ext.get(areaid);
    var elem_txt = elem.dom.innerHTML;
    var features = VIEW.mapLayers.MASASLayerEntry.getFeaturesByAttribute('areaid',
        areaid);
    if (features && features.length > 0) {
        if (elem_txt === '[Show]') {
            // there may be multiple polygon(s) and/or circle(s)
            for (var i = 0; i < features.length; i++) {
                features[i].renderIntent = 'revision';
            }
            console.debug('Show CAP area ' + areaid);
            elem.update('[Hide]');
        } else {
            for (var i = 0; i < features.length; i++) {
                features[i].renderIntent = 'delete';
            }
            console.debug('Hide CAP area ' + areaid);
            elem.update('[Show]');
        }
        VIEW.mapLayers.MASASLayerEntry.redraw();
    }
};


/**
 * Clean up when closing a content window.
 */
VIEW.close_content_window = function () {
    VIEW.mapLayers.MASASLayerEntry.destroyFeatures();
    // remove the row highlighting from the grid
    var row = VIEW.gridPanel.getStore().find('id',
        VIEW.contentWindow.panels.Atom.entryID);
    if (row > -1) {
        Ext.fly(VIEW.gridPanel.getView().getRow(row)).removeClass(
            'x-grid3-row-selected');
    }
    VIEW.contentWindow = null;
    
    //TODO: supporting previous method of cleaning up after closing
    //      a window, find out if needed anymore
    //VIEW.clean_up_selections();
};


/**
 * Used for viewing a revision for an Entry, or opening an Entry in its own
 * window.
 */
VIEW.show_entry_revision = function (url, secret) {
    console.debug('Entry revision window opened');
    if (!url || !secret) {
        console.error('Entry Revision URL or secret not found, unable to load');
        alert('Unable to load Entry');
        return;
    }
    var win_title = 'Entry';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 400,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel.Atom({
            template: VIEW.Template.Entry_Combined_Template,
            'secret': secret,
            'url': url,
            listeners: {
                'contentloaded': function (cmp) {
                    // add handlers to show popup windows with content
                    Ext.select('.CAPRevisionLink').on({
                        'click': VIEW.show_CAP_revision,
                        scope: cmp
                    });
                    Ext.select('.AttachmentRevisionLink').on({
                        'click': VIEW.show_attachment_revision,
                        scope: cmp
                    });
                }
            }
        })
    });
    win.items.itemAt(0).loadContent();
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * When viewing the Revision log, opens a new window to display an associated
 * CAP message for that revision.
 */
VIEW.show_CAP_revision = function (evt) {
    console.debug('CAP revision window opened');
    var el = evt.target;
    var capUrl;
    // stop the browser from following the provided link in href
    evt.stopEvent();
    // because of href previously viewed content will appear as already
    // having been viewed based on browser history.  The actual URL is
    // stored between the <a></a> tags
    if (el.text) {
        capUrl = el.text;
    } else if (el.innerText) {
        // IE 8 support
        capUrl = el.innerText;
    } else {
        console.error('CAP Revision URL not found, unable to load');
        alert('Unable to load CAP message');
        return;
    }
    var win_title = 'CAP Message';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 325,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel.CAP({
            // secret is available because the scope was set to the
            // Revisions panel
            'secret': this.secret,
            'url': capUrl
        })
    });
    win.items.itemAt(0).loadContent();
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * When viewing the Revision log, opens a new window to display an associated
 * Attachment for that revision.
 */
VIEW.show_attachment_revision = function (evt) {
    console.debug('Attachment revision window opened');
    var el = evt.target;
    var attachUrl;
    // stop the browser from following the provided link in href
    evt.stopEvent();
    // because of href previously viewed content will appear as already
    // having been viewed based on browser history.  The actual URL is
    // stored between the <a></a> tags
    if (el.text) {
        attachUrl = el.text;
    } else if (el.innerText) {
        // IE 8 support
        attachUrl = el.innerText;
    } else {
        console.error('Attachment Revision URL not found, unable to load');
        alert('Unable to load Attachment');
        return;
    }
    var attachData = {
        'url': attachUrl,
        //TODO: language placeholder for now
        'hrefLang': null,
        'title': el.getAttribute('data-title'),
        'mime': el.getAttribute('data-mime'),
        'length': ((el.getAttribute('data-length')) ? el.getAttribute('data-length') : 'N/A')
    };
    // use the OpenLayers MASAS module's regular expressions to decode the
    // enclosure mime-type into a simplified one for attachment presentation
    for (var type in OpenLayers.Format.MASASFeed.EnclosureRegEx) {
        if (attachData.mime.search(OpenLayers.Format.MASASFeed.EnclosureRegEx[type]) >= 0) {
            attachData['type'] = type;
            break;
        }
    }
    if (!attachData['type']) {
        attachData['type'] = 'unknown';
    }
    // image shows a thumbnail preview while external only shows an icon
    var panel_type = (attachData['type'] === 'image') ? 'Image' : 'External';
    var win_title = 'Attachment';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 325,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel[panel_type]({
            // secret is available because the scope was set to the
            // Revisions panel
            'secret': this.secret,
            'linkData': attachData
        })
    });
    win.items.itemAt(0).loadContent();
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Open a new window to show the Access Control details web page for an
 * author URI.
 */
VIEW.show_author = function (uri, name) {
    console.debug('Show author window opened');
    var secret = '';
    if (VIEW.contentWindow) {
        // use the secret that corresponds to the currently open Entry
        secret = VIEW.contentWindow.feedInfo.get('secret');
    } else {
        // using the first secret value available
        var first_feed = Ext.StoreMgr.get('FeedSources').getAt(0);
        secret = first_feed.get('secret');
    }
    var authUrl = Ext.urlAppend(uri, 'secret=' + secret);
    var win_title = 'Author Information';
    // the add button should only be available when viewing from an Entry
    // and not from the access log or other locations
    var add_button = {};
    if (name) {
        add_button = {
            text: 'Add',
            tooltip: 'Add to the Authors list',
            authorUri: uri,
            authorName: name,
            handler: function (btn) {
                var author_data = {
                    uri: btn.authorUri,
                    name: btn.authorName,
                    remove: true
                };
                var list_store = VIEW.settingsPanel.authorSettings.authorList.store;
                list_store.add(new list_store.recordType(author_data));
                VIEW.settingsPanel.authorSettings.authorList.refresh();
                console.debug('Added ' + author_data.name + ' to author list');
                Ext.Msg.alert('Success', 'Added ' + author_data.name);
                setTimeout( function () {
                    Ext.Msg.hide();
                }, 2000);
            }
        };
    }
    
    var win = new Ext.Window({
        title: win_title,
        'float': true,
        height: 500,
        width: 650,
        cls: 'AuthorInfo',
        renderTo: Ext.getBody(),
        layout: 'fit',
        html: '<iframe width="100%" height="100%" src="' + authUrl + '">',
        bbar: [ add_button ]
    });
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Used by AdvancedSettingsPanel.js  Allows the user to see all of the original geometry
 * for Entries.  For example, to show all road segments that may be closed.  
 */
VIEW.show_all_geometries = function () {
    VIEW.mapLayers.MASASLayerGeometries.destroyFeatures();
    var orig_features = VIEW.mapLayers.MASASLayer.features;
    var found_features = [];
    var orig_geom = null;
    var new_features = [];
    for (var i = 0; i < orig_features.length; i++) {
        // when clustering is turned on, most features end up as part
        // of a cluster, so look here first
        if (orig_features[i].cluster) {
            for (var j = 0; j < orig_features[i].cluster.length; j++) {
                found_features.push(orig_features[i].cluster[j]);
            }
        } else {
            found_features.push(orig_features[i]);
        }
    }
    for (var k = 0; k < found_features.length; k++) {
        if (found_features[k].originalGeom) {
            orig_geom = found_features[k].originalGeom.clone();
        } else {
            orig_geom = found_features[k].geometry.clone();
        }
        new_features.push(new OpenLayers.Feature.Vector(orig_geom));
    }
    VIEW.mapLayers.MASASLayerGeometries.addFeatures(new_features);
};


/**
 * Change the datetime values to account for timezones for display
 *
 * @param {DateTime} - a DateTime object to adjust
 * @param {Integer} - number of hours to adjust by, can accept minute
 *                    values such as 1.5  for 1 1/2 hours
 */
VIEW.adjust_time = function (date, offset) {
    var dt = date.clone();
    if (offset !== 0) {
        var hrOff = parseInt(offset, 10);
        var minOff = offset % hrOff * 60;
        //test for non-integer offsets
        if (minOff) {
            var min = dt.getMinutes() + minOff;
            dt.setMinutes(min);
        }
        var hr = dt.getHours() + hrOff;
        dt.setHours(hr);
    }
    return dt;
};


/**
 * Show Entry options window
 * 
 * @param {String} - URL for this Entry (required)
 * @param {String} - secret used to load this URL (required)
 */
VIEW.show_entry_options = function (url, secret, geom_type, geom_val) {
    if (!url || !secret) {
        console.error('Show entry options missing url or secret');
        return;
    }
    var option_buttons = [{
        xtype: 'button',
        text: 'Show in New Window',
        // width, height
        anchor: '100%, 20%',
        handler: function (btn) {
            // closing the options window first
            btn.ownerCt.close();
            VIEW.show_entry_revision(url, secret);
        }
    }, {
        xtype: 'button',
        text: 'Get Entry URL',
        anchor: '100%, 20%',
        handler: function (btn) {
            btn.ownerCt.close();
            var link_win_title = 'Entry URL';
            var link_win = new Ext.Window({
                title: link_win_title,
                layout: 'anchor',
                width: 500,
                height: 75,
                plain: true,
                items: [{
                    xtype: 'textfield',
                    height: 40,
                    anchor: '100%',
                    value: url,
                    listeners: {
                        afterrender: function () {
                            // select the text in this field after
                            // a 1/2 sec delay to allow it to display
                            this.focus(true, 500);
                        }
                    }
                }]
            });
            link_win_title += ' <a class="titleClose" href="#" ' +
                'onClick="Ext.getCmp(\'' + link_win.getId() +
                '\').close(); return false;">Close</a>';
            link_win.setTitle(link_win_title);
            link_win.show();
        }
    }];
    
    if (VIEW.EXPORT_ENTRY_URL) {
        option_buttons.push({
            xtype: 'button',
            text: 'Save Entry',
            anchor: '100%, 20%',
            handler: function (btn) {
                console.debug('Saving entry');
                btn.ownerCt.close();
                var save_win_title = 'Saving Entry...';
                var download_url = VIEW.EXPORT_ENTRY_URL + '?url=' + url +
                    '&secret=' + secret;
                var save_win = new Ext.Window({
                    title: save_win_title,
                    layout: 'fit',
                    height: 250,
                    width: 350,
                    draggable: false,
                    resizable: false,
                    modal: true,
                    cls: 'EntrySaveWindow',
                    html: '<div class="EntrySaveDiv">The Entry is being' +
                        ' downloaded, archived into a ZIP file, and you will' +
                        ' be prompted when its ready to be saved. Attachments' +
                        ' will increase the download time.</div>' +
                        //NOTE: this iframe doesn't have an ExtJS timeout, it
                        //      will time out when the server times out
                        ' <iframe width="325" height="125" frameBorder="0"' +
                        ' src="' + download_url + '">'
                });
                save_win_title += ' <a class="titleClose" href="#" ' +
                    'onClick="Ext.getCmp(\'' + save_win.getId() +
                    '\').close(); return false;">Close</a>';
                save_win.setTitle(save_win_title);
                save_win.show();
            }
        });
    }
    
    if (geom_type && geom_val) {
        option_buttons.push({
            xtype: 'button',
            text: 'Add Geometry as Layer',
            anchor: '100%, 20%',
            handler: function (btn) {
                console.debug('Adding entry geom as layer');
                btn.ownerCt.close();
                Ext.Msg.prompt('Add Map Layer', 'Enter a name for new layer:',
                    function (btn, text) {
                        if (btn === 'ok') {
                            VIEW.Layers.add_entry_layer(text, 'entry', url,
                                geom_type, geom_val);
                        }
                    }, null, false, 'Entry');
            }
        });
    }
    
    if (VIEW.FAVORITE_ENTRY_URL) {
        option_buttons.push({
            xtype: 'button',
            text: 'Add to Favorites',
            anchor: '100%, 20%',
            handler: function (btn) {
                console.debug('Adding entry to favorites');
                btn.ownerCt.close();
                Ext.Ajax.request({
                    url: VIEW.FAVORITE_ENTRY_URL,
                    params: { 'url': url, 'secret': secret },
                    success: function () {
                        Ext.Msg.alert('Success', 'Added to Favorites');
                    },
                    failure: function (response) {
                        console.error('Favorite error: ' + response.statusText);
                        Ext.Msg.alert('Error', 'Unable to add to Favorites');
                    }
                });
            }
        });
    }
    
    var win_title = 'Entry Options';
    var win = new Ext.Window({
        title: win_title,
        layout: 'anchor',
        width: 250,
        height: 250,
        plain: true,
        draggable: false,
        resizable: false,
        modal: true,
        items: option_buttons
    });
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};



//TODO: not sure why this is cleanup is needed as its expensive to
//      run and likely not necessary.  Currently not in use.
/**
 * This is called by the close listener on content windows.
 */
VIEW.clean_up_selections = function (cmp) {
    Ext.each(VIEW.mapLayers.MASASLayer.features, function (feat) {
        if (!feat.cluster && feat.layer) {
            //VIEW.mapControls.highlightControl.unhighlight(feat);
            this.removeFeatures([feat]);
            feat.destroy();
            return false;
        } else if (feat.layer && feat.cluster.length === 1 &&
        feat.cluster[0].renderIntent !== "default") {
            feat.cluster[0].renderIntent = "default";
        }
        this.redraw(true);
    }, VIEW.mapLayers.MASASLayer);
    VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
    VIEW.contentWindow = null;
};


/*TODO: likely can be removed.  In SVN Revision 192:193 madair committed the
 * addition of this to cleanup use of the drop-down arrows on the feed
 * loading button. The commit message was "re #4957: de-activate the cluster
 * strategy when removing features and reloading a single feed"
 * The goal seems to be to prevent duplicates and/or missing icons on the
 * map when you load a single feed, since the other feeds are left in place.
 * However, this was running for every feed load, including 'all feeds' where
 * there is a store wide clear already.  And after any feed load, filterFeatures
 * is being run to sync the store and the layer's features regardless.
 */
/**
 * Removes features from the MASASLayer based on their feed URL
 */
VIEW.clear_feed_features = function (feedUrl) {
    var features = VIEW.mapLayers.MASASLayer.features;
    var foundFeatures = [];
    var len = features.length;
    for (var i = 0; i < len; ++i) {
        if (features[i].attributes['source_url'] === feedUrl) {
            foundFeatures.push(features[i]);
        }
        for (var j = 0; j < features[i].cluster.length; ++j) {
            var feature = features[i].cluster[j];
            var cluster = [];
            if (feature.attributes['source_url'] === feedUrl) {
                foundFeatures.push(feature);
                //remove it from the cluster and push it up as an unclustered 
                //feature so it can be deleted in the removeFeatures call
                VIEW.mapLayers.MASASLayer.features.push(feature);
            } else {
                cluster.push(feature);
            }
        }
        features[i].cluster = cluster;
    }
    VIEW.mapLayers.MASASLayer.removeFeatures(foundFeatures);
};
