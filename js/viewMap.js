/**
MASAS View Tool - Map Setup
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings for the map.
*/

/*global VIEW,Ext,OpenLayers,GeoExt,google */

Ext.ns('VIEW');

/**
 * Default Map Options
 */
VIEW.mapOptions = {
    projection: new OpenLayers.Projection('EPSG:900913'),
    displayProjection: new OpenLayers.Projection('EPSG:4326'),
    theme: null,
    eventListeners: {
        'zoomend': VIEW.clean_up_map
    }
};

VIEW.map = new OpenLayers.Map(VIEW.mapOptions);

// setup the clustering strategy for MASAS Entries
VIEW.cluster = new OpenLayers.Strategy.Cluster({distance: 20, addTo: true});


/**
 * Setup of the Layer Styles
 */
(function() {
    
    // same values as EntryBackground in CSS
    VIEW.layerColours = {
        'red': {'background': 'bg-red.png', 'colour': '#FF0000'},
        'yellow': {'background': 'bg-yellow.png', 'colour': '#FFFF00'},
        'green': {'background': 'bg-green.png', 'colour': '#008000'},
        'gray': {'background': 'bg-gray.png', 'colour': '#696969'},
        'black': {'background': 'bg-black.png', 'colour': '#000000'},
        'blue': {'background': 'bg-blue.png', 'colour': '#0000FF'},
        'purple': {'background': 'bg-purple.png', 'colour': '#800080'}
    };
    
    var ClusterOptions = {
        context: {
            mySymbol: function (feature) {
                if (feature.attributes.count > 1) {
                    // a clustered point
                    //var href = OpenLayers.Util.removeTail(window.location.href);
                    //return href.slice(0,href.lastIndexOf("/")-href.length+1) + 'img/cluster.png';
                    return VIEW.ICON_LOCATION + 'cluster.png';
                } else if (feature.cluster) {
                    // all single points should go through this
                    return feature.cluster[0].attributes.icon;
                } else {
                    // last check
                    if (feature.attributes.icon) {
                        return feature.attributes.icon;
                    } else {
                        // default if nothing else
                        console.error('Unable to determine mySymbol icon');
                        return OpenLayers.Format.MASASFeed.IconURL + 'ems/other/small.png';
                    }
                }
            },
            clusterSize: function (feature) {
                if (feature.attributes.count > 1) {
                    var size = 7 + feature.attributes.count;
                    // may need to adjust this for very large numbers
                    return (size > 20) ? 20 : size;
                } else {
                    // base symbol size, used for both cluster icons and
                    // entry icons, its a radius so results in a 30x30 square
                    return 15;
                }
            },
            myLabel: function (feature) {
                if (feature.attributes.count > 1) {
                    // clusters are labelled with the count
                    return feature.attributes.count;
                } else {
                    return '';
                }
            },
            myBackground: function (feature) {
                var colour = null;
                var background = '';
                if (feature.attributes.count > 1) {
                    // no background on clusters
                    return '';
                } else if (feature.cluster) {
                    // normal single point
                    if (feature.cluster[0].attributes.colour) {
                        colour = feature.cluster[0].attributes.colour.toLowerCase();
                    }
                } else {
                    // last check
                    if (feature.attributes.colour) {
                        colour = feature.attributes.colour.toLowerCase();
                    }
                }
                if (colour) {
                    if (VIEW.layerColours[colour]) {
                        background = VIEW.ICON_LOCATION +
                            VIEW.layerColours[colour]['background'];
                    }
                }
                return background;
            }
        }
    };
    
    var GeometryOptions = {
        context: {
            myStrokeWidth: function (feature) {
                if (!feature.cluster && feature.geometry &&
                feature.geometry instanceof OpenLayers.Geometry.LineString) {
                    // make lines easier to see by being thicker
                    return 5;
                }
                // 1 is the default as it helps outline other shapes
                return 1;
            },
            myStrokeColor: function (feature) {
                if (!feature.cluster && feature.geometry &&
                feature.geometry instanceof OpenLayers.Geometry.LineString) {
                    // lines are shown in their respective colour
                    if (feature.attributes.colour) {
                        var colour = feature.attributes.colour.toLowerCase();
                        if (VIEW.layerColours[colour]) {
                            return VIEW.layerColours[colour]['colour'];
                        }
                    }
                }
                // black is the default for all other geometries as its
                // used to outline the fill colour
                return '#000000';
            },
            myFillColor: function (feature) {
                if (feature.attributes.colour) {
                    var colour = feature.attributes.colour.toLowerCase();
                    if (VIEW.layerColours[colour]) {
                        return VIEW.layerColours[colour]['colour'];
                    }
                }
                // green is the default
                return '#008000';
            }
        }
    };
    
    VIEW.layerStyles = {
        // normal display of a cluster
        'small_cluster_style': new OpenLayers.Style({
            graphicOpacity: 0.7,
            externalGraphic: '${mySymbol}',
            pointRadius: '${clusterSize}',
            label: '${myLabel}',
            labelAlign: 'lm',
            labelXOffset: -4,
            labelYOffset: 0,
            fontColor: 'black',
            fontSize: '12px',
            fontWeight: 'bold',
            fontFamily: 'Arial, Helvetica, sans-serif;',
            backgroundGraphic: '${myBackground}',
            // slightly larger than the 30x30 icon
            backgroundHeight: 32,
            backgroundWidth: 32
        }, ClusterOptions),
        // alter some values when hovering over a cluster to expand it
        'big_cluster_style': new OpenLayers.Style({
            pointRadius: 20,
            fillOpacity: 0.9,
            strokeColor: 'red',
            strokeOpacity: 0.9
        }),
        // Entry geometry layers
        'geometry_style': new OpenLayers.Style({
            fillOpacity: 0.5,
            fillColor: '${myFillColor}',
            strokeOpacity: 0.7,
            strokeColor: '${myStrokeColor}',
            strokeWidth: '${myStrokeWidth}'
        }, GeometryOptions)
    };
    
})();


/**
 * Permanent Map Layers
 */
VIEW.mapLayers = VIEW.mapLayers || {};

// User location
VIEW.mapLayers.location = new OpenLayers.Layer.Vector('Location', {
        displayInLayerSwitcher: false
    });

// this layer shows the actual icons and cluster symbols
VIEW.mapLayers.MASASLayer = new OpenLayers.Layer.Vector('MASAS Feed', {
        //TODO: why does this start out as false?
        visibility: false,
        strategies: [VIEW.cluster],
        styleMap: new OpenLayers.StyleMap({
            "default": VIEW.layerStyles.small_cluster_style,
            "select": VIEW.layerStyles.big_cluster_style,
            "delete": {display: 'none'}
        }),
        eventListeners: {
            featureselected: VIEW.feature_selected,
            featureunselected: VIEW.feature_unselected,
            featuresadded: function (evtObj) {
                // updates the icon locations for all non-point geometries
                // after a feed load or other change
                //TODO: enhance this function so it can be passed the
                //      instance attributesfor the map, etc via this function
                //      call instead of checking for global values.
                VIEW.gridPanel.featureStore.proxy.protocol.format.checkGeometry({onLoad: true});
            },
            featureModified: function (evtObj) {
                //TODO: not sure if this is ever called, just here in case?
                Ext.Msg.alert('Modified', 'Feature modified');
            }
        }
    });

// display the geometry of an entry during a hover/popup
VIEW.mapLayers.MASASLayerHighlight = new OpenLayers.Layer.Vector('MASAS Highlight', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style
        })
    });

// display the geometry of an entry when the content window is open
VIEW.mapLayers.MASASLayerEntry = new OpenLayers.Layer.Vector('MASAS Entry', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style,
            // minor style change for revision geometries
            'revision': OpenLayers.Util.applyDefaults(
                {'fillColor': '#FF00FF', 'strokeColor': '#FF00FF'},
                VIEW.layerStyles.geometry_style)
        })
    });

// displaying all feature geometries with no icons or popups
VIEW.mapLayers.MASASLayerGeometries = new OpenLayers.Layer.Vector('MASAS Geometries', {
        visibility: false,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style
        })
    });

// displays a bounding box of the feed loading request area
VIEW.mapLayers.loadFeed = new OpenLayers.Layer.Vector('Load Feed', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fill: false,
                strokeColor: 'red',
                strokeOpacity: 0.5,
                strokeDashstyle: 'dot'
            })
        })
    });


/**
 * Initial Map Controls
 */
VIEW.mapControls = VIEW.mapControls || {};

// created here in order to reference VIEW.mapLayers above
VIEW.mapControls.highlightControl = new OpenLayers.Control.SelectFeature(
    VIEW.mapLayers.MASASLayer, {
        // mouse users can hover or click while touch users can only click
        hover: (VIEW.TOUCH_ENABLE) ? false : true,
        autoActivate: true
    });

VIEW.mapControls.loadingControl = new OpenLayers.Control.LoadingPanel();

VIEW.mapControls.navHistory = new OpenLayers.Control.NavigationHistory();

VIEW.mapControls.attribution = new OpenLayers.Control.Attribution();


/**
 * Initialize remainder of map functionality once all files loaded and ready
 */
VIEW.initialize_map = function () {
    VIEW.map.addLayers(Ext.getValues(VIEW.mapLayers));
    VIEW.map.addControls(Ext.getValues(VIEW.mapControls));
    VIEW.MapTools.initialize();
    VIEW.MapLayers.initialize();
};
