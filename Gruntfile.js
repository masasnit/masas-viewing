//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  // All upfront config goes in a massive nested object.
  grunt.initConfig({
    // You can set arbitrary key-value pairs.
    distFolder: 'js',
    stagingFolder: 'stage',
    cssFolder: 'css',
    templateFolder: 'template',
    devFolder : 'dev',
    deployFolder : "deploy",
    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),
    // Grunt tasks are associated with specific properties.
    // these names generally match their npm package name.
      'template': {
          'production-html-template': {
              'options': {
                  'data': {

						'topCSS' : '<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/ext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/GeoExt-1.2rc/css/geoext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/css/view-min.css" rel="stylesheet" type="text/css"/>',
									
						'topJS' : '<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>\n'+
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/view-plugins-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/xml-json-min.js" type="text/javascript"></script>\n' +
									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/js/view-overrides-min.js" type="text/javascript"></script>',
									
						'bottomJS' : '<script src="./<%= deployFolder %>/js/view-debug.js" type="text/javascript"></script>',

                      

                  }
              },
              'files': {
                  'index.html': ['<%= templateFolder %>/index.html.tpl']
              }
          },
          'development-html-template': {
              'options': {
                  'data': {
                  		'topCSS' : '<link href="libs/ExtJS-3.4.2/resources/css/ext-all.css" rel="stylesheet" type="text/css" />\n' + 
                  					'<link href="libs/ExtJS-3.4.2/examples/ux/css/GroupTab.css" rel="stylesheet" type="text/css" />\n' +
                  					'<link href="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/css/fileuploadfield.css" type="text/css" rel="stylesheet">\n' +
                  					'<link href="libs/ExtJS-3.4.2/extensions/css/FilterRow.css" rel="stylesheet" type="text/css" />\n' +
                  					'<link href="libs/OpenLayers-2.13.1/theme/default/style.css" rel="stylesheet" type="text/css" />\n' +
                  					'<link href="libs/GeoExt-1.2rc/resources/css/geoext-all.css" rel="stylesheet" type="text/css" />' +
                  					'<link href="css/view.css" rel="stylesheet" type="text/css" />',
                  					
                  		'topJS' : '<script src="libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/RowExpander.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/GroupTab.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/GroupTabPanel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/Reorderer.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/ToolbarReorderer.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/FileUploadField.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/extensions/FilterRow.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/OpenLayers.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/extensions/LoadingPanel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/extensions/MASAS.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/extensions/OWM.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/GeoExt.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/extensions/OverviewMapPanel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/extensions/ClusterSelectionModel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/extensions/Measure.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/extensions/GoogleStreetViewPanel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/json2.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/JsonXml.js" type="text/javascript"></script>\n' +
                  					'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n' +
                  					'<script src="js/view-overrides.js" type="text/javascript"></script>',
                  					
						'bottomJS' : '<script src="js/viewTemplates.js" type="text/javascript"></script>\n' +
										'<script src="js/src/ViewComponents.js" type="text/javascript"></script>\n' +
										'<script src="js/src/StoreBoundButton.js" type="text/javascript"></script>\n' +
										'<script src="js/src/FeedGrid.js" type="text/javascript"></script>\n' +
										'<script src="js/src/FeedSettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/UserSettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/AuthorSettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/TimeSettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/AdvancedSettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/SettingsPanel.js" type="text/javascript"></script>\n' +
										'<script src="js/src/ContentPanelTypes.js" type="text/javascript"></script>\n' +
										'<script src="js/src/ContentDisplay.js" type="text/javascript"></script>\n' +
										'<script src="js/src/FeedFeatureStore.js" type="text/javascript"></script>\n' +
										'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
										'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
										'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
										'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
										'<script src="js/viewMap.js" type="text/javascript"></script>\n' +
										'<script src="js/view.js" type="text/javascript"></script>'
                  }
              },
              'files': {
                  'index.html': ['<%= templateFolder %>/index.html.tpl']
              }
          }
      },
      'shell': {
        'build': {
            'options': {
                'stdout': true
            },
            'command': ['cd build', 'python build.py'].join('&&')
        }
    }
  }); // The end of grunt.initConfig

  // We've set up each task's configuration.
  // Now actually load the tasks.
  // This will do a lookup similar to node's require() function.
  grunt.loadNpmTasks('grunt-template');
  grunt.loadNpmTasks('grunt-shell');
  // Register our own custom task alias.
  grunt.registerTask('prod-build', ['shell:build','template:production-html-template']);
  grunt.registerTask('dev-build', ['template:development-html-template']);
};